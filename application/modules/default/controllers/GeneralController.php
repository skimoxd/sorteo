<?

class Default_GeneralController extends Zend_Controller_Action
{
	protected $array_javascripts=array(
        "Jquery"=>"/javascripts/jquery-3.1.1.min.js",
        "Bootstrap"=>"/javascripts/bootstrap.min.js",
        "Bootstrap2"=>"/javascripts/Bootstrap4/bootstrap.min.js",
        "login"=>"/javascripts/login.js",
        "main"=>"/javascripts/main.js",
        "nav"=>"/javascripts/nav.js",
        "validator"=>"/javascripts/validator.js",
        "backstretch"=>"/javascripts/jquery.backstretch.min.js",
        "perfil"=>"/javascripts/perfil.js",
        "boleto"=>"/javascripts/catalogos/boleto.js",
        "rifa"=>"/javascripts/catalogos/rifa.js",
        "usuario"=>"/javascripts/catalogos/usuario.js",
        "cuenta"=>"/javascripts/catalogos/cuenta.js",
        "abono"=>"/javascripts/catalogos/abono.js",
        "tabla"=>"/javascripts/tabla.js",
        "reportes"=>"/javascripts/reportes.js",
        "bootbox"=>"/javascripts/bootbox.min.js",
        "chosen"=>"/javascripts/chosen.js",
        "jqueryDatatable"=>"/javascripts/jquery.dataTables.min.js",
        "autocomplete"=>"/javascripts/jquery-ui.min.js",
        "Datatable"=>"/javascripts/jquery.dataTables.min.js",
        "Responsive1"=>"/javascripts/dataTables.responsive.min.js",
        "datepicker"=>"/javascripts/bootstrap-datetimepicker.min.js",
        "datepicker_es"=>"/javascripts/bootstrap-datepicker.es.min.js",
        "Responsive2"=>"/javascripts/dataTables.bootstrap.min.js",
        "Principal"=>"/javascripts/principal.js",
        "General"=>"/javascripts/general.js"
	);
	protected $array_estilos=array(
        "Bootstrap"=>"/stylesheets/Bootstrap/bootstrap.css",
        "Bootstrap2"=>"/stylesheets/Bootstrap4/bootstrap.css",
        "Responsive"=>"/stylesheets/Bootstrap/responsive.bootstrap.min.css",
        "fonts"=>"/stylesheets/fonts.css",
        "layout"=>"/stylesheets/layout.css",
        "mapa"=>"/stylesheets/mapa.css",
        "api_maps"=>"/stylesheets/ol.css",
        "nav"=>"/stylesheets/nav.css",
        "dashboard"=>"/stylesheets/dashboard.css",
        "layout"=>"/stylesheets/layout.css",
        "chosen1"=>"/stylesheets/bootstrap-chosen.css",
        "chosen2"=>"/stylesheets/bootstrap-chosen-variables.less",
        "chosen3"=>"/stylesheets/bootstrap-chosen-variables.scss",
        "chosen4"=>"/stylesheets/bootstrap-chosen.less",
        "chosen5"=>"/stylesheets/bootstrap-chosen.scss",
        "Datatable"=>"/stylesheets/Bootstrap/dataTables.bootstrap.min.css",
        "responsive-data"=>"/stylesheets/responsive.dataTables.min.css",
        "datepicker"=>"/stylesheets/Bootstrap/bootstrap-datepicker.min.css",
        "jquery-table"=>"/stylesheets/jquery.dataTables.min.css",
        "stacktable"=>"/stylesheets/stacktable.css",
        "Login"=>"/stylesheets/login.css",
        "principal"=>"/stylesheets/principal.css",
        "time"=>"/stylesheets/jquery.timepicker.css"
	);
    protected $tabs=array(array("titulo"=>"Activado","onclick"=>"general.obtenerDatos(1);return false;"),array("titulo"=>"Desactivado","onclick"=>"general.obtenerDatos(0);return false;"));
    protected $alphanum="";
    protected $bootstrapantiguo=true;
    public function init()
    {
        //ini_set('display_errors', 1);
        error_reporting(E_ALL & ~E_DEPRECATED & ~E_WARNING );
        $this->alphanum = "AaBbCcDd123456789EeFfGgHhIiJ0918273645564738291jKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
       
        if($this->bootstrapantiguo){
            $javascripts=array("Jquery","Bootstrap","General","nav","Jqueryformtowiz","chosen","bootbox");
            $estilos=array("Responsive","Bootstrap","fonts","nav","layout","chosen1","chosen2","chosen3","chosen4","chosen5","stacktable");
        }
        else
        {
             $javascripts=array("Jquery","General","nav","Jqueryformtowiz","chosen","bootbox");
            $estilos=array("Responsive","fonts","nav","layout","chosen1","chosen2","chosen3","chosen4","chosen5","stacktable");
        }
        $this->CargarScripts($javascripts,$estilos);
        $this->codificartabla();
        
    }
   /* private function cargarpermisos(){
        $permisos=explode("|",$this->user_info->permiso);
        $datos=array();
        foreach($permisos as $p){
            $permiso=explode(",",$p);
            $datos[$permiso[0]]=$permiso[1];
        }
        $this->view->permisos=$datos;
    }*/
    public function nohtml(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
    }
    private function codificartabla(){
        $this->view->iniciar_javascript.="general.sProcessing='".$this->view->setcode("Procesando...")."';
            general.sLengthMenu='".$this->view->setcode("Mostrar _MENU_ registros")."';
            general.sZeroRecords='".$this->view->setcode("No se encontraron resultados")."';
            general.sEmptyTable='".$this->view->setcode("Ningun dato disponible en esta tabla")."';
            general.sInfo='".$this->view->setcode("Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros")."';
            general.sInfoEmpty='".$this->view->setcode("Mostrando registros del 0 al 0 de un total de 0 registros")."';
            general.sInfoFiltered='".$this->view->setcode("(filtrado de un total de _MAX_ registros)")."';
            general.sInfoPostFix='".$this->view->setcode("")."';
            general.sSearch='".$this->view->setcode("Buscar:")."';
            general.sUrl='".$this->view->setcode("")."';
            general.sInfoThousands='".$this->view->setcode(",")."';
            general.sLoadingRecords='".$this->view->setcode("Cargando...")."';
            general.sFirst='".$this->view->setcode("Primero")."';
            general.sLast='".$this->view->setcode("Último")."';
            general.sNext='".$this->view->setcode("Siguiente")."';
            general.sPrevious='".$this->view->setcode("Anterior")."';
            general.sSortAscending='".$this->view->setcode(": Activar para ordenar la columna de manera ascendente")."';
            general.sSortDescending='".$this->view->setcode(": Activar para ordenar la columna de manera descendente")."';";
    }
    protected function Desencriptar($cadena,$name='')
    {
        if($name==''){
            $token = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('token'));                     
            $token=$token->getIdentity();
            $name=$token['token'];
        }
        $suma = 0;
        $newtexto = "";
        for($x=0; $x < strlen($name); $x++)
        {  
            $suma += ord($name[$x]);
        }
        $semilla = "$suma";
        $suma = 0;
        for($z=0; $z < strlen($semilla); $z++)
        {
           $suma += $semilla[$z];
        }
        $semilla = $suma;
        for($y=0; $y < strlen($cadena); $y++)
        {
            if(ord($cadena[$y]) - $semilla > 31)
            {
               $suma = (ord($cadena[$y]) - $semilla);
            }
            else
            {
                $suma = 126 - (31 - (ord($cadena[$y]) - $semilla));
            } 
            $newtexto .= chr($suma);
        }
        return $newtexto;
    }
    protected function CargarScripts($javascripts,$estilos){
        if(!empty($javascripts))
            foreach($javascripts as $script)
                if(array_key_exists($script,$this->array_javascripts))
                    $this->view->headScript()->appendFile($this->array_javascripts[$script]);
        if(!empty($estilos))
            foreach($estilos as $script)
                if(array_key_exists($script,$this->array_estilos))
                    $this->view->headLink()->appendStylesheet($this->array_estilos[$script]);   
    }
    protected function formarArrayComplete($datos,$id,$busqueda)
    {   
        $arreglo="[";
        if(count($datos)>0){
            foreach($datos as $d){
                $arreglo.='{"value":"'.$d->$id.'","label":"'.utf8_decode(str_replace("\"","'",$d->$busqueda)).'"},';
            }
            $arreglo=substr($arreglo,0,-1);
        }
        $arreglo.="]";
        return $arreglo;
    }
    protected function listado(){
        if($this->identyfied()){
            $this->_helper->viewRenderer('/contenido/listado',null,true);
            $str="Default_Model_DbTable_".$this->modelo;
            $modelo = new $str();
            $estado=null;
            if($this->getRequest()->isXmlHttpRequest()){
                $this->view->isAjax = true;
                $this->_helper->layout()->disableLayout();
                $estado= intval($this->_request->getParam('estado'));
                if ($estado == 0){
                    $this->lista_columnas["desactivado_desde"]="Desactivado desde";
                }
            }
            //if($this->residuo)
                //$data=(is_null($estado))?$modelo->busqueda(1,null,1):$modelo->busqueda($estado,null,1);
            //else
                $data=(is_null($estado))?$modelo->busqueda():$modelo->busqueda($estado);
            $this->view->lista = $data;
            $this->view->listado = $data;
            $this->view->columnas=$this->lista_columnas;
            $this->view->controller=$this->modelo;
            $this->view->estado = $estado;
            $this->view->titulo = $this->titulo;
            $this->view->titulo_alta = utf8_encode($this->titulo_alta);
            //$this->view->identificador=$this->identificador;
            $this->view->extra_columnas=$this->extra_columnas;
        }
        else
            $this->_redirect('/login');
    }

    protected function obtenerFormulario($id,$token=false,$boleto=false,$rifas=false,$estados=false,$cuentas=false){
        if($this->identyfied()){
            $this->_helper->layout()->disableLayout();
                $this->_helper->viewRenderer('/'.$this->modelo.'/formulario',null,true);
            $str="Default_Model_DbTable_".$this->modelo;
            $modelo = new $str();
            if($rifas){
                $modelo_rifa= new Default_Model_DbTable_Rifa();
                $this->view->rifas=$modelo_rifa->obtenerActivos();
            }
            if($token)
                $this->view->token=$this->getToken();
            if($boleto){
                $modelo_estados= new Default_Model_DbTable_Boleto();
                $this->view->boleto=$modelo->comprobarboleto();
            }
            if($estados){
                $modelo_estados= new Default_Model_DbTable_Estado();
                $this->view->estados=$modelo_estados->busqueda();
            }
            if($cuentas){
                $modelo_cuenta= new Default_Model_DbTable_Cuenta();
                $this->view->cuentas=$modelo_cuenta->busqueda(1);
            }
            if($id>0){
                $this->view->editar=1;
                $this->view->data=$modelo->busqueda(false,$id);
            }
        }
        else
            $this->_redirect('/login');
        //echo $this->view->headScript();
    }
    protected function guardar($datos,$id){
        if($this->identyfied()){
            $this->nohtml();
            $modelo="Default_Model_DbTable_".$this->modelo;
            $modelo=new $modelo();
            if(empty($id))
                $datos["estado"]=1;
            //$validator = new PHPValidador();
            //if($validator->GeneralValidator($data,$req))
            if(true)
                try{
                    return ($modelo->guardar($datos,$id));
                }
                catch(Exception $e){
                     //"error"
                    return $e->getMessage();
                }
            else{
                return "error";
            }
         }
        else
            $this->_redirect('/login');
    }
    protected function identyfied(){
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('rifa'));
        if(!$auth->hasIdentity()){
            return false;
        }else{
            $this->user_info = $auth->getIdentity(); 
            $this->view->userinfo=$this->user_info;
            //$this->cargarpermisos();
            return true;
        }
    }
    protected function getToken(){
        $auth = Zend_Auth::getInstance();
        $auth->setStorage(new Zend_Auth_Storage_Session('token'));
        $storage = $auth->getStorage();
        $token = md5(substr(md5(str_shuffle($this->alphanum)), 0, 10));
        $data['token']=$token;
        $storage->write($data);
        return $data['token'];
    }
    protected function cambiarestado($id){
        $this->nohtml();
        $str="Default_Model_DbTable_".$this->modelo;
        $modelo = new $str();
        echo $modelo->cambiarestado($id);
    }
} ?>