<?
require_once('GeneralController.php');
class Default_AbonoController extends Default_GeneralController
{
	protected $lista_columnas=array("id"=>"id_abono","sorteo"=>"Sorteo","numeroboleto"=>"Número del boleto","costo_boleto"=>"Costo del boleto","descripcion"=>"Descripción","importe"=>"Importe");
    protected $titulo="Listado de Abonos";
    protected $titulo_alta = "Añadir Abono";
    protected $modelo="Abono";
    protected $extra_columnas=null;
    public function init()
    {
        parent::init();
        $javascripts=array("jqueryDatatable","Datatable","tabla","validator","Responsive1","Responsive2","abono","datepicker","datepicker_es");
        $estilos=array("Datatable","Responsive","datepicker");
        $this->CargarScripts($javascripts,$estilos);
        $this->identyfied();
        /*if($this->view->userinfo->tipo!=1)
            throw new Exception('Error 404 Pagina no encontrada.');*/
    }

    public function indexAction()

    {
       	parent::listado();
        $this->_helper->viewRenderer('/contenido/listadoabonos',null,true);
        $this->view->iniciar_javascript.="general.cargartabla();";
        $modelo_rifa= new Default_Model_DbTable_Rifa();
        $rifas=$modelo_rifa->obtenerActivos()->toArray();
        $this->view->headScript()->appendScript("$(document).ready(function(){
                 abono.rifas=".json_encode($rifas).";
                });");
	}
	public function obtenerformularioAction(){
		$id=$this->_request->getParam('id');
        parent::obtenerFormulario($id,false,false,true,false,true);
        $modelo_abonos= new Default_Model_DbTable_Abono();
        if($id)
            $this->view->boletos=$modelo_abonos->obtenerboletosapartados($this->view->data->id_sorteo,$this->view->data->id_boleto);

        
	}
    public function guardarAction(){
        $this->nohtml();
        if($this->identyfied()){
            $datos=$this->_request->getParam('datos');
            $modelo=new Default_Model_DbTable_Abono;
            $db = $modelo->getAdapter();
            // Iniciamos la transaccion
            $db->beginTransaction();
            try{
                $id=$datos["id_abono"];
                unset($datos["id_abono"]);
                unset($datos["id_sorteo"]);
                if($id!="0"){
                    $datos["fecha"]=new Zend_Db_Expr("STR_TO_DATE('".$datos["fecha"]."','%d/%m/%Y')");
                    $id_abono=$modelo->guardar($datos,$id);
                    if($datos["importe"]>0){
                        if($modelo->Comprobarimporte($datos["id_boleto"],$datos["importe"],$id_abono))
                        {
                            throw new Exception ("El importe excedio el monto total del boleto");
                        }
                    }
                    else
                        throw new Exception ("El importe no puede tener valor 0");
                    $modelo->Actualizarboleto($datos["id_boleto"],$id_abono);
                    $boleto=$modelo->ObtenerBoleto($datos["id_boleto"]);
                    if($boleto->estado==3 && !empty($boleto->archivo) ){
                       $modelo->ProcesoEstado($datos["id_boleto"]);
                    }
                }
                else
                {
                    if( array_key_exists("abono_sorteo",$datos)){
                       
                        if(!is_array($datos["abono_sorteo"])){
                            $datos["abono_sorteo"]=array($datos["abono_sorteo"]);
                            $datos["abono_boleto"]=array($datos["abono_boleto"]);
                            $datos["abono_cantidad"]=array($datos["abono_cantidad"]);
                        }
                        if(is_array($datos["abono_sorteo"])){
                            foreach($datos["abono_sorteo"] as $i=>$v){
                                $datos_ab["fecha"]=new Zend_Db_Expr("STR_TO_DATE('".$datos["fecha"]."','%d/%m/%Y')");
                                $datos_ab["id_boleto"]=$datos["abono_boleto"][$i];
                                $datos_ab["importe"]=$datos["abono_cantidad"][$i];
                                $datos_ab["id_cuenta"]=$datos["id_cuenta"];
                                $datos_ab["descripcion"]=$datos["descripcion"];
                                $datos_ab["archivo"]="";
                                $id_abono=$modelo->guardar($datos_ab,$id);
                                if($datos_ab["importe"]>0){
                                    if($modelo->Comprobarimporte($datos_ab["id_boleto"],$datos_ab["importe"],$id_abono))
                                    {
                                        throw new Exception ("El importe excedio el monto total del boleto");
                                    }
                                }
                                else
                                    throw new Exception ("El importe no puede tener valor 0");
                                $modelo->Actualizarboleto($datos_ab["id_boleto"],$id_abono);
                                $boleto=$modelo->ObtenerBoleto($datos_ab["id_boleto"]);
                                if($boleto->estado==3 && !empty($boleto->archivo) ){
                                   $modelo->ProcesoEstado($datos_ab["id_boleto"]);
                                }
                            }
                        }
                    }
                }
                $db->commit();
                echo json_encode(array("tipo"=>"1","mensaje"=>$datos));
            }
            catch(Exception $e){
                $db->rollBack();
                //echo $e->getMessage();//"error";
                echo json_encode(array("tipo"=>"2","mensaje"=>$e->getMessage()));
            }
        }
        else
        {
            $this->nohtml();
            echo json_encode(array("tipo"=>"4","mensaje"=>""));
        }
    }

    public function cambiarestadoAction(){
        $this->nohtml();
        $this->nohtml();
        if($this->identyfied()){
            $id=$this->_request->getParam('id');
            $modelo=new Default_Model_DbTable_Abono;
            $db = $modelo->getAdapter();
            // Iniciamos la transaccion
            $db->beginTransaction();
            try{
                $abono=$modelo->busqueda(false,$id);
                if($abono->estado==1){
                    $modelo->desactivar($id);
                    $modelo->actualizarboleto($abono->id_boleto);
                }
                else
                {
                    $modelo->activar($id);
                    $modelo->actualizarboleto($abono->id_boleto);
                    $importe=$modelo->obtenerimporte($abono->id_abono);
                    if($modelo->Comprobarimporte($abono->id_boleto,$importe,$abono->id_abono))
                    {
                        throw new Exception ("El importe excedio el monto total del boleto");
                    }
                }
                $db->commit();
                echo json_encode(array("tipo"=>"1","mensaje"=>$id));
            }
            catch(Exception $e){
                $db->rollBack();
                //echo $e->getMessage();//"error";
                echo json_encode(array("tipo"=>"2","mensaje"=>$e->getMessage()));
            }
        }
        else
        {
            $this->nohtml();
            echo json_encode(array("tipo"=>"4","mensaje"=>""));
        }
        //$modelo->actualizarestado($id);
    }

    public function validarpagoAction(){
        $this->nohtml();
        $importe=$this->_request->getParam('importe');
        $id=$this->_request->getParam('id');
        $id_abono=$this->_request->getParam('id_abono');
        $modelo=new Default_Model_DbTable_Abono();
        if($id_abono!=0)
            $get=$modelo->Comprobarimporte($id,$importe,$id_abono);
        else
            $get=$modelo->Comprobarimporte($id,$importe);
        if($get)
            echo 1;
        else 
            echo 0;
    }
    public function obtenerboletosAction(){
        $this->nohtml();
        $id=$this->_request->getParam('id');
        $modelo=new Default_Model_DbTable_Boleto();
        $boletos=$modelo->obtenerboletosapartados($id)->toArray();
        echo json_encode($boletos);
    }
    public function subirarchivoAction(){
        $this->nohtml();
        $target_dir = "Archivos/";
        $id_abono=$this->_request->getPost('abono');
        $carpeta=$target_dir;
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
        $nombre=$_FILES["archivo"]["name"];
        $nombre=explode(".",$nombre);
        $ext=$nombre[count($nombre)-1];
        $name = "abono".$id_abono."_".md5(substr(md5(str_shuffle($this->alphanum)), 0, 10));
        $ruta = $carpeta . $name.".".$ext;
        if (move_uploaded_file($_FILES["archivo"]["tmp_name"], $ruta)) {
            $modelo=new Default_Model_DbTable_Abono;
            $modelo->guardararchivo($ruta,$id_abono);
            echo  "1";
            
           
        } else {
            echo "Lo sentimos, hubo un error subiendo el archivo.";
        }
    }
 
} ?>