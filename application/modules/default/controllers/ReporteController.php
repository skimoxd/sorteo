<?
require_once('GeneralController.php');
class Default_ReporteController extends Default_GeneralController
{  
    public function init()
    {   

        
        parent::init();
        $javascripts=array("datepicker","datepicker_es","reportes","autocomplete","moment");
        $estilos=array("datepicker","select","Responsive");
        $this->CargarScripts($javascripts,$estilos);
    }

    public function indexAction()
    {
       
	}
    public function boletosAction(){
        if($this->identyfied()){
            $modelo=new Default_Model_DbTable_Rifa;
            $this->view->rifa=$modelo->ObtenerActivos();
            $this->view->headScript()->appendScript("$(document).ready(function(){
                    reportes.iniciar();
                });");
        }
        else
            $this->_redirect('/login'); 
    }
    public function abonosAction(){
        if($this->identyfied()){
            $this->view->headScript()->appendScript("$(document).ready(function(){
                    reportes.iniciar();
                });");
        }
        else
            $this->_redirect('/login'); 
    }
    public function obtenerreporteAction(){
        $this->nohtml();
        $id_sorteo=$this->_request->getParam('id_sorteo');

        // Crea un nuevo objeto PHPExcel
        $objPHPExcel = new PHPExcel();
                // Create a new worksheet called "My Data"
        $myWorkSheet0 = new PHPExcel_Worksheet($objPHPExcel, 'Boletos de rifa');

        // Attach the "My Data" worksheet as the first worksheet in the PHPExcel object
        $objPHPExcel->addSheet($myWorkSheet0, 0);

        // Establecer propiedades
        $objPHPExcel->getProperties()
        ->setCreator("Cattivo")
        ->setLastModifiedBy("Cattivo")
        ->setTitle("Reporte de boletos pagados de rifa")
        ->setSubject("Documento Excel de Prueba")
        ->setDescription("Reporte de boletos por rifa.")
        ->setKeywords("Excel Office 2007 openxml php")
        ->setCategory("Excel");

        // Tabla de gastos
        $modelo=new Default_Model_DbTable_Boleto;
        $datos=$modelo->obtenerBoletos($id_sorteo);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Rifa')
                ->setCellValue('B1', 'Numero de Boleto')
                ->setCellValue('C1', 'Nombre Completo')
                ->setCellValue('D1', 'Telefono')
                ->setCellValue('E1', 'Correo')
                ->setCellValue('F1', 'Estado')
                ->setCellValue('G1', 'Ciudad')
                ->setCellValue('H1', 'Estado del Boleto');
        $renglon=2;
            foreach($datos as $d){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->SetCellValue('A'.$renglon, $d->rifa)
                    ->SetCellValue('B'.$renglon, $d->num_boleto)
                    ->SetCellValue('C'.$renglon, $d->nombre_completo)
                    ->SetCellValue('D'.$renglon, $d->telefono)
                    ->SetCellValue('E'.$renglon, $d->correo)
                    ->SetCellValue('F'.$renglon, $d->edo_nombre)
                    ->SetCellValue('G'.$renglon, $d->ciudad)
                    ->SetCellValue('H'.$renglon, $d->edo_bol);
                $renglon++;
                }
            
        $objPHPExcel->getActiveSheet()->setTitle('Boletos');

        // Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Boletos pagados.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
    public function obtenerreporte2Action(){
        $this->nohtml();
        $fecha1=$this->_request->getParam('fecha1');
        $fecha2=$this->_request->getParam('fecha2');

        // Crea un nuevo objeto PHPExcel
        $objPHPExcel = new PHPExcel();
                // Create a new worksheet called "My Data"
        $myWorkSheet0 = new PHPExcel_Worksheet($objPHPExcel, 'Abonos');

        // Attach the "My Data" worksheet as the first worksheet in the PHPExcel object
        $objPHPExcel->addSheet($myWorkSheet0, 0);

        // Establecer propiedades
        $objPHPExcel->getProperties()
        ->setCreator("Cattivo")
        ->setLastModifiedBy("Cattivo")
        ->setTitle("Reporte de abono a cuentas")
        ->setSubject("Documento Excel de Abonos")
        ->setDescription("Reporte estado de cuenta.")
        ->setKeywords("Excel Office 2007 openxml php")
        ->setCategory("Excel");

        // Tabla de gastos
        $modelo=new Default_Model_DbTable_Abono;
        $datos=$modelo->obtenerAbono($fecha1,$fecha2);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Referencia')
                ->setCellValue('B1', 'Banco')
                ->setCellValue('C1', 'Fecha de Abono')
                ->setCellValue('D1', 'Numero del Boleto')
                ->setCellValue('E1', 'Descricpion del Abono')
                ->setCellValue('F1', 'Importe');
        $renglon=2;
            foreach($datos as $d){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->SetCellValue('A'.$renglon, $d->num_ref)
                    ->SetCellValue('B'.$renglon, $d->banco)
                    ->SetCellValue('C'.$renglon, $d->fecha)
                    ->SetCellValue('D'.$renglon, $d->num_boleto)
                    ->SetCellValue('E'.$renglon, $d->descripcion)
                    ->SetCellValue('F'.$renglon, $d->importe);
                $renglon++;
                }
            
        $objPHPExcel->getActiveSheet()->setTitle('Abonos');

        // Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Abonos.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
    public function crearReporte($tabla){
        header("Content-Type: application/vnd.ms-excel");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("content-disposition: attachment;filename=Reportes.xls"); 
        echo $tabla;

    } 
}?>