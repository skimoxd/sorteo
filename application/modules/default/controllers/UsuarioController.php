<?
require_once('GeneralController.php');
class Default_UsuarioController extends Default_GeneralController
{
	protected $lista_columnas=array("id"=>"id_usuario","nombre_completo"=>"Nombre","usuario"=>"Usuario","correo"=>"Correo");
    protected $titulo="Listado de Usuarios";
    protected $titulo_alta = "Añadir Usuario";
    protected $modelo="Usuario";
    protected $extra_columnas=null;
    public function init()
    {
        parent::init();
        $javascripts=array("jqueryDatatable","Datatable","tabla","validator","Responsive1","Responsive2","usuario");
        $estilos=array("Datatable","Responsive");
        $this->CargarScripts($javascripts,$estilos);
        $this->identyfied();
        /*if($this->view->userinfo->tipo!=1)
            throw new Exception('Error 404 Pagina no encontrada.');*/
    }

    public function indexAction()

    {
       	parent::listado();
        $this->view->iniciar_javascript.="general.cargartabla();";
	}
	public function obtenerformularioAction(){
		$id=$this->_request->getParam('id');
        parent::obtenerFormulario($id,true,false);
        
	}
    public function guardarAction(){
        $this->nohtml();
        if($this->identyfied()){
            $datos=$this->_request->getParam('datos');
            $modelo=new Default_Model_DbTable_Usuario;
            $id=$datos["id_usuario"];
            if(isset($datos["contrasena"]))
                $datos["contrasena"]=md5($this->Desencriptar($datos["contrasena"]));
            unset($datos["id_usuario"]);
            $mensaje=parent::guardar($datos,$id);
            if(is_numeric ( $mensaje))
                echo json_encode(array("tipo"=>"1","mensaje"=>$mensaje));
            else
                echo json_encode(array("tipo"=>"2","mensaje"=>$mensaje));
                
        }
        else
            echo json_encode(array("tipo"=>"4","mensaje"=>""));
    }
    public function existeAction(){
        $this->nohtml();
        $id=$this->_request->getParam('id');
        $usuario=$this->_request->getParam('usuario');
        $modelo=new Default_Model_DbTable_Usuario();
        if($modelo->comprobarUsuario($id,$usuario))
            echo 1;
        else 
            echo 2;
    }
    public function existeemailAction(){
        $this->nohtml();
        $id=$this->_request->getParam('id');
        $email=$this->_request->getParam('email');
        $modelo=new Default_Model_DbTable_Usuario();
        if($modelo->comprobarEmail($id,$email))
            echo 1;
        else 
            echo 2;
    }
    public function cambiarestadoAction(){
        $id=$this->_request->getParam('id');
        parent::cambiarestado($id);
    }
 
} ?>