<?
require_once('GeneralController.php');
class Default_PrincipalController extends Default_GeneralController
{
    public function init()
    {
    	$this->bootstrapantiguo=false;
    	$this->view->SetLayout('principal');
        parent::init();
        $javascripts=array("Bootstrap2","Principal","validator");
        $estilos=array("Bootstrap2","principal");
        $this->CargarScripts($javascripts,$estilos);
    }

	public function indexAction()
    {
        $modelo=new Default_Model_DbTable_Rifa;
        $rifas=$modelo->obtenerRifasActivas();
        $this->view->rifas=$rifas;
        $this->view->boletosrifa=array();
        foreach($rifas as $rifa)
        {
            $this->view->boletosrifa[$rifa->id_sorteo]=$modelo->obtenerboletos($rifa->id_sorteo);
        }
    }

    public function guardarAction(){

    }

	} ?>