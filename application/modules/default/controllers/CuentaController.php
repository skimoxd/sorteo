<?
require_once('GeneralController.php');
class Default_CuentaController extends Default_GeneralController
{
	protected $lista_columnas=array("id"=>"id_cuenta","Numero_referencia"=>"Referencia","Banco"=>"Banco");
    protected $titulo="Listado de Cuentas";
    protected $titulo_alta = "Añadir Cuenta";
    protected $modelo="Cuenta";
    protected $extra_columnas=null;
    public function init()
    {
        parent::init();
        $javascripts=array("jqueryDatatable","Datatable","tabla","validator","Responsive1","Responsive2","cuenta","datepicker","datepicker_es");
        $estilos=array("Datatable","Responsive","datepicker");
        $this->CargarScripts($javascripts,$estilos);
        $this->identyfied();
        /*if($this->view->userinfo->tipo!=1)
            throw new Exception('Error 404 Pagina no encontrada.');*/
    }

    public function indexAction()

    {
       	parent::listado();
        $this->view->iniciar_javascript.="general.cargartabla();";
	}
	public function obtenerformularioAction(){
		$id=$this->_request->getParam('id');
        parent::obtenerFormulario($id,false,false);
        
	}
    public function guardarAction(){
        if($this->identyfied()){
            $datos=$this->_request->getParam('datos');
            $modelo=new Default_Model_DbTable_Rifa;
            $id=$datos["id_cuenta"];
            unset($datos["id_cuenta"]); 
            $mensaje=parent::guardar($datos,$id);
            if(is_numeric ( $mensaje))
                echo json_encode(array("tipo"=>"1","mensaje"=>$mensaje));
            else
                echo json_encode(array("tipo"=>"2","mensaje"=>$mensaje));
                
        }
        else
        {
            $this->nohtml();
            echo json_encode(array("tipo"=>"4","mensaje"=>""));
        }
    }

    /*public function existeAction(){
        $this->nohtml();
        $id=$this->_request->getParam('id');
        $rifa=$this->_request->getParam('rifa');
        $modelo=new Default_Model_DbTable_Rifa();
        if($modelo->comprobarRifa($id,$rifa))
            echo 1;
        else 
            echo 2;
    }*/
    public function cambiarestadoAction(){
        $id=$this->_request->getParam('id');
        parent::cambiarestado($id);
    }
 
} ?>