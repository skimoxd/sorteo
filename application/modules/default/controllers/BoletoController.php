<?
require_once('GeneralController.php');
class Default_BoletoController extends Default_GeneralController
{
    protected $lista_columnas=array("id"=>"id_boleto","rifa"=>"Sorteo","num_boleto"=>"numero del boleto","nombre_completo"=>"Nombre completo");
    protected $titulo="Listado de boletos";
    protected $titulo_alta = "Añadir Boleto";
    protected $modelo="Boleto";
    protected $extra_columnas=null;
    private $api;
    //protected $identificador=4;

   public function init()
    {
        parent::init();
        $javascripts=array("jqueryDatatable","Datatable","tabla","validator","Responsive1","Responsive2","boleto");
        $estilos=array("Datatable","Responsive");
        $this->CargarScripts($javascripts,$estilos);
    }

    public function indexAction()

    {
        parent::listado();
        $this->_helper->viewRenderer('/contenido/listadoboletos',null,true);
        $this->view->iniciar_javascript.="general.cargartabla();";
    }
    public function obtenerformularioAction(){
        $id=$this->_request->getParam('id');
        parent::obtenerFormulario($id,false,false,true,true);
        $modelo_boletos= new Default_Model_DbTable_Boleto();
        if($id)
            $this->view->boletos=$modelo_boletos->obtenerboletosdisponibles($this->view->data->id_sorteo,$this->view->data->num_boleto);

        $modelo_boletos= new Default_Model_DbTable_Boleto();
        $this->view->ladas=$modelo_boletos->obtenerLadas();
    }
    public function pedirboletoAction(){
        $this->_helper->layout()->disableLayout();
        $modelo_estados= new Default_Model_DbTable_Estado();
        $this->view->estados=$modelo_estados->busqueda();
        $modelo_boletos= new Default_Model_DbTable_Boleto();
        $this->view->ladas=$modelo_boletos->obtenerLadas();
    }

    public function guardarboletoAction(){
        $this->nohtml();
        $datos=$this->_request->getParam('datos');
        $modelo=new Default_Model_DbTable_Boleto;
        $db = $modelo->getAdapter();
        $telefono="";
        // Iniciamos la transaccion
        $db->beginTransaction();
        try{
            $boletos="";
            $num_b=$datos["num_b"];
            unset($datos["num_b"]);
            $telefono=$datos["telefono"];
            $datos["id_sorteo"]=$datos["id_s"];
            unset($datos["id_s"]);
            if(!is_array($num_b))
                $num_b=array($num_b);
            foreach($num_b as $b){
                if($modelo->ComprobarBoleto(0,$b,$datos["id_sorteo"]))
                    throw new Exception ("Este boleto ya esta ocupado");
                $datos["num_boleto"]=$b;
                $id_boleto=$modelo->guardar($datos,null);

                $cantidad=$modelo->obtenerCantidad($datos["id_sorteo"]);
                $repeticiones=(int)(10000/$cantidad);

                for($x=0;$x<$repeticiones;$x=$x+1){
                    $oportunidad["num_oportunidad"]=$datos["num_boleto"]+($x*$cantidad);
                    $oportunidad["id_boleto"]=$id_boleto;
                    $modelo->guardaroportunidad($oportunidad);
                }
                $this->api=$modelo->obtenerapi();
                $rifa=$modelo->obtenerrifa(true,$datos["id_sorteo"])->descripcion;
                if($boletos!='')
                    $boletos.=','.$datos["num_boleto"];
                else
                    $boletos=$datos["num_boleto"];
                
               
            } 
            $num=$modelo->obtenernumlada($datos["id_lada"]);
            $mensaje=str_replace("#470#", $boletos, $this->api->mensaje);
            $mensaje=str_replace("#460#", $rifa, $mensaje);
            
            $to=$num.$telefono;
            if($this->enviarmensaje($to,$mensaje)===false){
                $db->commit();
                echo json_encode(array("tipo"=>"3","mensaje"=>$this->api->msj_whats));
            }
            else{
                $db->commit();
                echo json_encode(array("tipo"=>"1","mensaje"=>$datos));
            }
            
        }
        catch(Exception $e){
            $db->rollBack();
            //echo $e->getMessage();//"error";
            echo json_encode(array("tipo"=>"2","mensaje"=>$e->getMessage()));
        }
    }
    private function enviarmensaje($to,$msg){
        $data = [
            'phone' => $to, //Numero que recibira el msj
            'body' => $msg
        ];
        $json = json_encode($data); 
        $url = $this->api->url.'sendMessage?token='.$this->api->token;
        $options = stream_context_create(['http' => [
                'method' => 'POST',
                'header' => 'Content-type: application/json',
                'content' => $json
            ],"ssl"=>[         
                "verify_peer"=>false,         
                "verify_peer_name"=>false,     
            ]
        ]);

        $result = file_get_contents($url, false, $options);

        if ($result) return json_decode($result);
        return false;
       
    }
    public function guardarAction(){
        $this->nohtml();
        if($this->identyfied()){
            $datos=$this->_request->getParam('datos');
            $modelo=new Default_Model_DbTable_Boleto;
            $db = $modelo->getAdapter();
            // Iniciamos la transaccion
            $db->beginTransaction();
            try{
                $id=$datos["id_boleto"];
                unset($datos["id_boleto"]);
                if($modelo->ComprobarBoleto($id,$datos["num_boleto"],$datos["id_sorteo"]))
                    throw new Exception ("Este boleto ya existe");

                $id_boleto=$modelo->guardar($datos,$id);
                
                $cantidad=$modelo->obtenerCantidad($datos["id_sorteo"]);
                $repeticiones=(int)(10000/$cantidad);
                $modelo->borraroportunidades($id_boleto);
                for($x=0;$x<$repeticiones;$x=$x+1){
                    $oportunidad["num_oportunidad"]=$datos["num_boleto"]+($x*$cantidad);
                    $oportunidad["id_boleto"]=$id_boleto;
                    $modelo->guardaroportunidad($oportunidad);
                }
                $db->commit();
                echo json_encode(array("tipo"=>"1","mensaje"=>$datos));
            }
            catch(Exception $e){
                $db->rollBack();
                //echo $e->getMessage();//"error";
                echo json_encode(array("tipo"=>"2","mensaje"=>$e->getMessage()));
            }
        }
        else
        {
            $this->nohtml();
            echo json_encode(array("tipo"=>"4","mensaje"=>""));
        }
    }

    public function cambiarestadoAction(){
        $id=$this->_request->getParam('id');
        parent::cambiarestado($id);
    }    
    public function obtenerboletosAction(){
        $this->nohtml();
        $id=$this->_request->getParam('id');
        $sorteo=$this->_request->getParam('sorteo');
        $boleto=$this->_request->getParam('boleto');
        $modelo=new Default_Model_DbTable_Boleto();
        $boletos=$modelo->obtenerboletosdisponibles($id,null);
        $res=array();
        $opcion="";
        foreach($boletos as $b){
            $opcion.='<option value="'.$b.'"">'.$b.'</option>';
        }
        $res["opcion"]=$opcion;
        echo json_encode($res);
    }
    public function existeAction(){
        $this->nohtml();
        $id=$this->_request->getParam('id');
        $sorteo=$this->_request->getParam('sorteo');
        $boleto=$this->_request->getParam('boleto');
        $modelo=new Default_Model_DbTable_Boleto();
        if($modelo->ComprobarBoleto($id,$boleto,$sorteo))
            echo 1;
        else 
            echo 2;
    }
    public function altaAction(){
        $this->nohtml();
        $datos=json_decode($this->_request->getPost('data'));
        $modelo=new Default_Model_DbTable_Boleto;
        $db = $modelo->getAdapter();
        // Iniciamos la transaccion
        $db->beginTransaction();
        try{
            
            foreach($datos as $v){
                if(!$modelo->existeproducto($v->codigo1)){
                    $prod["codigo1"]=$v->codigo1;
                    $prod["codigo2"]=$v->codigo2;
                    $prod["descripcion"]=str_replace("'","\'",($v->descripcion));
                    $modelo->guardarproducto($prod);
                }
            }
            //$db->rollBack();
            $db->commit();
            echo "1";
        }
        catch(Exception $e){
            $db->rollBack();
            echo "2";
        }
    }
    public function subirarchivoAction(){
        $this->nohtml();
        $target_dir = "Archivos/";
        $id_boleto=$this->_request->getPost('boleto');
        $carpeta=$target_dir;
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
        $nombre=$_FILES["archivo"]["name"];
        $nombre=explode(".",$nombre);
        $ext=$nombre[count($nombre)-1];
        $name = "boleto".$id_boleto."_".md5(substr(md5(str_shuffle($this->alphanum)), 0, 10));
        $ruta = $carpeta . $name.".".$ext;
        if (move_uploaded_file($_FILES["archivo"]["tmp_name"], $ruta)) {
            $modelo=new Default_Model_DbTable_Boleto;
            $modelo->guardararchivo($ruta,$id_boleto);
            echo  "1";
            
           
        } else {
            echo "Lo sentimos, hubo un error subiendo el archivo.";
        }
    }
}
?>