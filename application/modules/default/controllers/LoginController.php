<?
require_once('GeneralController.php');
class Default_LoginController extends Default_GeneralController
{

    public function init()
    {
        parent::init();
        $this->view->SetLayout('login');
        $javascripts=array("validator","login","backstretch");
        $estilos=array("Login");
        $this->CargarScripts($javascripts,$estilos);
    }

    public function indexAction()
    {
        $itoken = Zend_Auth::getInstance();
        $itoken->setStorage(new Zend_Auth_Storage_Session('token'));
        if($itoken->hasIdentity()){
            if($this->_request->getParam('usuario')){
                $usuario= $this->_request->getParam('usuario');
                $contrasena= $this->_request->getParam('contrasena');   
                $token=$itoken->getIdentity();
                $contrasena = parent::Desencriptar($contrasena, $token['token']);
                $itoken->clearIdentity();
                /*autenticacion*/
                $zend_auth = Zend_Auth::getInstance();          
                $zend_auth->setStorage(new Zend_Auth_Storage_Session('rifa'));
                $dbAdapter = Zend_Db_Table_Abstract::getDefaultAdapter();
                $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter, 'usuario', 'usuario', 'contrasena');
                $authAdapter
                    ->setIdentity($usuario)
                    ->setCredential(md5($contrasena));         
                $result = $zend_auth->authenticate($authAdapter);               
                /*validacion*/
                if($result->isValid()){        
                    $storage = $zend_auth->getStorage();
                    $modelo_u = new Default_Model_DbTable_Usuario();
                    $datos = $modelo_u->obtenerDatos($usuario,md5($contrasena));                      
                    if($datos){
                        $storage->write($datos);       
                        $redirector = new Zend_Controller_Action_Helper_Redirector(); 
                        $redirector->gotoUrl('/index');                  

                    }else{  
                        $zend_auth = Zend_Auth::getInstance(); 
                        $zend_auth->setStorage(new Zend_Auth_Storage_Session('rifa'));             
                        $zend_auth->clearIdentity();
                        $this->view->token = parent::getToken();
                        $this->view->message = "<div class='alert alert-error'>Usuario o Contraseña incorrectos</div>";                    
                        
                    }
                }else{
                    $this->view->token = parent::getToken();
                    $this->view->message = "<div class='alert alert-error text-danger'>Usuario o Contraseña incorrectos</div>";                        
                }
            }else{
                $this->view->token = parent::getToken();
            }
        }
        else{
            $this->view->token = parent::getToken();
        }
	
    }
    public function logoutAction(){
        $zend_auth = Zend_Auth::getInstance();
        $zend_auth->setStorage(new Zend_Auth_Storage_Session('rifa'));
        $zend_auth->clearIdentity();
        $redirector = new Zend_Controller_Action_Helper_Redirector();
        $redirector->gotoUrl('/login');
    }
}?>