<?
class Default_ErrorController extends Zend_Controller_Action
{
    public function init()
    {
        $this->initView();
 
        //$this->view->setLayout('error');
    }
    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
 
        $this->view->mensaje= '';
         
        switch ($errors->type) {
         
            //Error MVC
             
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
             
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
             
                //devolveremos un código 404 de página inexistente
                 
                $this->getResponse()->setHttpResponseCode(404);
                 
                //Lo que no existe es el controller
                 
                if ($errors->type == Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER) {
                 
                    $this->view->mensaje= 'No se ha encontrado el Controller: ';
                     
                    $this->view->mensaje.= $errors->request->getControllerName();
                }
                if ($errors->type == Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION) {
         
                    $this->view->mensaje= ' No se ha encontrado el Action: ';
                     
                    $this->view->mensaje.= $errors->request->getActionName();
                 
                }
             
            break;
                 
            //No es un error MVC
                 
            default:
                 
                //se cogenlas excepciones que han saltado
                 
                $errores = $this->getResponse()->getException();
             
                //se trata cada tipo de excepcion
                 
                foreach ($errores as $error){
         
                    switch(get_class($error)){
                     
                        case 'Zend_Db_Statement_Exception':
                         
                        $this->view->mensaje = 'ERROR DE BBDD: '.$error->getMessage();
                         
                        break;
                         
                        case 'Zend_View_Exception':
                         
                        $this->view->mensaje = 'ERROR EN LA VISTA: '.$error->getMessage();
                         
                        break;
                         
                        case 'Zend_Loader_Exception':
                         
                        $this->view->mensaje = 'ERROR AL CARGAR: '.$error->getMessage();
                         
                        break;
                         
                        case 'Zend_Form_Exception':
                         
                        $this->view->mensaje = 'ERROR EN EL FORMULARIO: '.$error->getMessage();
                         
                        break;
                         
                        //cualquier error no especifico
                         
                        default:
                         
                         echo $error->getMessage();
                        $this->view->mensaje= 'Error desconocido!';
                         
                        break;
                     
                    } //end switch(get_class($error))
         
                }
         
            break; //terminar con los errores no MVC
         
        }
        echo  $this->view->mensaje;
         
        /* PARA GUARDAR TODOS LOS DATOS EN UN ARCHIVO DE ERRORES (LOG)
         
        $log = new Zend_Log(new Zend_Log_Writer_Stream(Globals::getConfig()->dirs->tmp.'Error.log'));
         
        $log->debug($errors->exception->getMessage() . "\n" . $errors->exception->getTraceAsString());
         
        */
         
        return;
    }
}?>