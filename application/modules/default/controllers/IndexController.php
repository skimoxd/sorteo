<?
require_once('GeneralController.php');
class Default_IndexController extends Default_GeneralController
{   

    public function init()
    {
        parent::init();
        $javascripts=array("perfil","Validator");
        $estilos=array("dashboard");
        $this->CargarScripts($javascripts,$estilos);
    }

    public function indexAction()
    {
        if($this->identyfied()){
            $modelo=new Default_Model_DbTable_Rifa;
            $rifas=$modelo->obtenerRifasActivas();
            $this->view->rifas=$rifas;
            $this->view->boletosrifa=array();
            foreach($rifas as $rifa)
            {
                $this->view->boletosrifa[$rifa->id_sorteo]=$modelo->obtenerboletos($rifa->id_sorteo);
            }
        }
        else
            $this->_redirect('/principal');
	} 
    

} ?>