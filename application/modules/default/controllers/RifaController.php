<?
require_once('GeneralController.php');
class Default_RifaController extends Default_GeneralController
{
	protected $lista_columnas=array("id"=>"id_sorteo","descripcion"=>"Nombre del sorteo","fecha_inicio"=>"fecha de inicio","fecha_final"=>"Fecha de finalizacion");
    protected $titulo="Listado de Rifas";
    protected $titulo_alta = "Añadir Rifa";
    protected $modelo="Rifa";
    protected $extra_columnas=null;
    public function init()
    {
        parent::init();
        $javascripts=array("jqueryDatatable","Datatable","tabla","validator","Responsive1","Responsive2","rifa","datepicker","datepicker_es");
        $estilos=array("Datatable","Responsive","datepicker");
        $this->CargarScripts($javascripts,$estilos);
        $this->identyfied();
        /*if($this->view->userinfo->tipo!=1)
            throw new Exception('Error 404 Pagina no encontrada.');*/
    }

    public function indexAction()

    {
       	parent::listado();
        $this->view->iniciar_javascript.="general.cargartabla();";
	}
	public function obtenerformularioAction(){
		$id=$this->_request->getParam('id');
        parent::obtenerFormulario($id,true,false);
        
	}
    public function guardarAction(){
        if($this->identyfied()){
            $datos=$this->_request->getParam('datos');
            $modelo=new Default_Model_DbTable_Rifa;
            $id=$datos["id_sorteo"];
            unset($datos["id_sorteo"]); 
            $datos["fecha_inicio"]=new Zend_Db_Expr("STR_TO_DATE('".$datos["fecha_inicio"]."','%d/%m/%Y')");
            $datos["fecha_final"]=new Zend_Db_Expr("STR_TO_DATE('".$datos["fecha_final"]."','%d/%m/%Y')");
            $mensaje=parent::guardar($datos,$id);
            if(is_numeric ( $mensaje))
                echo json_encode(array("tipo"=>"1","mensaje"=>$mensaje));
            else
                echo json_encode(array("tipo"=>"2","mensaje"=>$mensaje));
                
        }
        else
        {
            $this->nohtml();
            echo json_encode(array("tipo"=>"4","mensaje"=>""));
        }
    }

    /*public function existeAction(){
        $this->nohtml();
        $id=$this->_request->getParam('id');
        $rifa=$this->_request->getParam('rifa');
        $modelo=new Default_Model_DbTable_Rifa();
        if($modelo->comprobarRifa($id,$rifa))
            echo 1;
        else 
            echo 2;
    }*/
    public function cambiarestadoAction(){
        $id=$this->_request->getParam('id');
        parent::cambiarestado($id);
    }
    public function subirarchivoAction(){
        $this->nohtml();
        $target_dir = "imgsorteo/";
        $id_sorteo=$this->_request->getPost('sorteo');
        $carpeta=$target_dir;
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
        $nombre=$_FILES["archivo"]["name"];
        $name=$nombre;
        $nombre=explode(".",$nombre);
        $ext=$nombre[count($nombre)-1];
        //$name=$nombre;
        //$name = "sorteo".$id_sorteo."_".md5(substr(md5(str_shuffle($this->alphanum)), 0, 10));
        $ruta = $carpeta . $name;
        if (move_uploaded_file($_FILES["archivo"]["tmp_name"], $ruta)) {
            $modelo=new Default_Model_DbTable_Rifa;
            $modelo->guardararchivo($ruta,$id_sorteo);
            echo  "1";
            
           
        } else {
            echo "Lo sentimos, hubo un error subiendo el archivo.";
        }
    }
 
} ?>