<?
class Zend_View_helper_General extends Zend_View_Helper_Abstract{

    /**
    *Alan Zazueta
    cambia de layout
    *public
    *@param $layout string
    */
    public function general(){
        return $this;
    }
    public function creartabla(Zend_Db_Table_Rowset_Abstract $rowset,$columnas,$extra)
    {
        $id='';
        $controller=Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
        $table= '<table class="dTable table table-striped table-bordered dt-responsive nowrap" padding="0" text-align="center" width="100%" cellspacing="0"><thead>';
        //$table= '<table class="dTable table table-bordered"><thead>';
        $textra="";
        foreach($columnas as $indice=>$columna) {

            if($indice!="id")
                $table .= $this->cabecera(utf8_encode($columna));
        }
        $table .= $this->cabecera("Editar");
        if(count($rowset)>0){
            $table .= '</thead><tbody>';
            foreach($rowset as $row) {
                
                $table .= '<tr>';
            //Dropdown Test
                $edit = "";
                $del = "";
                $id=$columnas['id'];
                $edit = "<a data-id=".$row->$id." title='Editar' onclick='general.obtenerFormulario(".$row->$id.")' class='fancybox' style='cursor:pointer'><img src='/imagenes/iconos/pencil.png' /></a>";
                $del = "<a data-id=".$row->$id." title='".(($row->estado==0)?'Activar':'Desactivar')."' onclick='general.cambiarestado(".$row->$id.",this)' data-estado=".$row->estado." class='inactive' style='cursor:pointer' ><img src='/imagenes/iconos/".(($row->estado==0)?'tick_circle.png':'delete.png')."'  /></a>";

                foreach($columnas as $indice=>$columna) {
                    if($indice!="id")
                       $table .= $this->celda($row,$indice);
                }
                if($extra!=null){
                    $detalles='<div title="Detalles" class="details-control" data-id="'.$row->$columnas['id'].'"></div>';
                    $table .= "<td>".$detalles." ".$edit." ".$del."</td>";
                }
                else
                {
                    $table .= "<td style=\"text-align:center;\">".$edit." ".$del."</td>";
                }
                $table .= '</tr>';
                
                //$textra.='<div class="divinvisible" id="extra_'.$row->$columnas['id'].'"><div class="extrainfo" ">';
                if($extra!=null){
                    foreach($extra as $indice=>$columna) {
                        if($indice=="telefonos"){
                            $telefonos=explode("|",$row->telefonos);
                            foreach($telefonos as $telefono){
                                $textra.="<div>
                                    <div>".$columna.":</div>
                                    <div>".$telefono."</div>
                                </div>";
                            }
                        }
                        else
                            $textra.="<div>
                                <div>".$columna.":</div>
                                <div>".$row->$indice."</div>
                            </div>";
                    }
                   
                }
                $textra.='</div></div>';
            }
            $table .='</tbody></table>';
        }
        else
            $table .= '</thead></table>';
        
        return $table."".$textra;
    }
    public function creartablaBoletos(Zend_Db_Table_Rowset_Abstract $rowset,$columnas,$extra)
    {
        $id='';
        $controller=Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
        $table= '<table class="dTable table table-striped table-bordered dt-responsive nowrap" padding="0" text-align="center" width="100%" cellspacing="0"><thead>';
        //$table= '<table class="dTable table table-bordered"><thead>';
        $textra="";
        foreach($columnas as $indice=>$columna) {

            if($indice!="id")
                $table .= $this->cabecera(utf8_encode($columna));
        }
        $table .= $this->cabecera("Editar");
        if(count($rowset)>0){
            $table .= '</thead><tbody>';
            foreach($rowset as $row) {
                
                $table .= '<tr>';
            //Dropdown Test
                $edit = "";
                $del = "";
                $id=$columnas['id'];
                if($row->estado!=4)
                    $edit = "<a data-id=".$row->$id." title='Editar' onclick='general.obtenerFormulario(".$row->$id.")' class='fancybox' style='cursor:pointer'><img src='/imagenes/iconos/pencil.png' /></a>";
                if($row->estado==1){
                    $del = "<a data-id=".$row->$id." title='".(($row->estado==0)?'Activar':'Desactivar')."' onclick='general.cambiarestado(".$row->$id.",this)' data-estado=".$row->estado." class='inactive' style='cursor:pointer' ><img src='/imagenes/iconos/".(($row->estado==0)?'tick_circle.png':'delete.png')."'  /></a>";
                }
                foreach($columnas as $indice=>$columna) {
                    if($indice!="id")
                       $table .= $this->celda($row,$indice);
                }
                if($extra!=null){
                    $detalles='<div title="Detalles" class="details-control" data-id="'.$row->$columnas['id'].'"></div>';
                    $table .= "<td>".$detalles." ".$edit." ".$del."</td>";
                }
                else
                {
                    $table .= "<td style=\"text-align:center;\">".$edit." ".$del."</td>";
                }
                $table .= '</tr>';
                
                //$textra.='<div class="divinvisible" id="extra_'.$row->$columnas['id'].'"><div class="extrainfo" ">';
                if($extra!=null){
                    foreach($extra as $indice=>$columna) {
                        if($indice=="telefonos"){
                            $telefonos=explode("|",$row->telefonos);
                            foreach($telefonos as $telefono){
                                $textra.="<div>
                                    <div>".$columna.":</div>
                                    <div>".$telefono."</div>
                                </div>";
                            }
                        }
                        else
                            $textra.="<div>
                                <div>".$columna.":</div>
                                <div>".$row->$indice."</div>
                            </div>";
                    }
                   
                }
                $textra.='</div></div>';
            }
            $table .='</tbody></table>';
        }
        else
            $table .= '</thead></table>';
        
        return $table."".$textra;
    }
    public function creartablaAbonos(Zend_Db_Table_Rowset_Abstract $rowset,$columnas,$extra)
    {
        $id='';
        $controller=Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
        $table= '<table class="dTable table table-striped table-bordered dt-responsive nowrap" padding="0" text-align="center" width="100%" cellspacing="0"><thead>';
        //$table= '<table class="dTable table table-bordered"><thead>';
        $textra="";
        foreach($columnas as $indice=>$columna) {

            if($indice!="id")
                $table .= $this->cabecera(utf8_encode($columna));
        }
        $table .= $this->cabecera("Editar");
        if(count($rowset)>0){
            $table .= '</thead><tbody>';
            foreach($rowset as $row) {
                
                $table .= '<tr>';
            //Dropdown Test
                $edit = "";
                $del = "";
                $id=$columnas['id'];
                $edit = "<a data-id=".$row->$id." title='Editar' onclick='general.obtenerFormulario(".$row->$id.")' class='fancybox' style='cursor:pointer'><img src='/imagenes/iconos/pencil.png' /></a>";
                $del = "<a data-id=".$row->$id." title='".(($row->estado==0)?'Activar':'Desactivar')."' onclick='abono.cambiarestado(".$row->$id.",this)' data-estado=".$row->estado." class='inactive' style='cursor:pointer' ><img src='/imagenes/iconos/".(($row->estado==0)?'tick_circle.png':'delete.png')."'  /></a>";
            
                foreach($columnas as $indice=>$columna) {
                    if($indice!="id")
                       $table .= $this->celda($row,$indice);
                }
                if($extra!=null){
                    $detalles='<div title="Detalles" class="details-control" data-id="'.$row->$columnas['id'].'"></div>';
                    $table .= "<td>".$detalles." ".$edit." ".$del."</td>";
                }
                else
                {
                    $table .= "<td style=\"text-align:center;\">".$edit." ".$del."</td>";
                }
                $table .= '</tr>';
                
                //$textra.='<div class="divinvisible" id="extra_'.$row->$columnas['id'].'"><div class="extrainfo" ">';
                if($extra!=null){
                    foreach($extra as $indice=>$columna) {
                        if($indice=="telefonos"){
                            $telefonos=explode("|",$row->telefonos);
                            foreach($telefonos as $telefono){
                                $textra.="<div>
                                    <div>".$columna.":</div>
                                    <div>".$telefono."</div>
                                </div>";
                            }
                        }
                        else
                            $textra.="<div>
                                <div>".$columna.":</div>
                                <div>".$row->$indice."</div>
                            </div>";
                    }
                   
                }
                $textra.='</div></div>';
            }
            $table .='</tbody></table>';
        }
        else
            $table .= '</thead></table>';
        
        return $table."".$textra;
    }
    private function cabecera($titulo){
        return '<th style="text-align:center;">'.$this->view->setcode($titulo).'</th>';
    }
    private function celda($row,$indice,$campo=null){
        $value=null;
       // switch($indice){
             //case 'desactivado_desde':$value=$this->view->Setformat($row->$indice,2);break;
             //case 'date':$value=$this->view->Setformat($row->$indice,1,false);break;
        //}
        return '<td style="text-align:center;">'.$this->view->setcode($this->view->validatedate((($value!==null)?$value:(($campo!==null)?$campo:$row->$indice)))).'</td>';
    }
   
    
}?>