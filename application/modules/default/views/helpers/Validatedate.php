<?
class Zend_View_Helper_Validatedate extends Zend_View_Helper_Abstract
{
	/**
	*Alan Zazueta
	comprueba si el objecto esta vacio para evitar errores al querer ver la variable de un objecto vacio
	*public
	*@param $list object 
	*@param $field string 
	*@return value
	*/
	private $formatos=array(
        'd/m/Y g:i:s a'=>'Y-m-d H:i:s',
        'd/m/Y'=>'Y-m-d');
    public function validatedate($date)
	{
		foreach($this->formatos as $indice=>$formato){
			$d = DateTime::createFromFormat($formato, $date);
	    	if($d && $d->format($formato) == $date){
	    		$d = DateTime::createFromFormat($formato, $date);
	    		return $d->format($indice);
	    	}
		}
		return $date;
	}

}
?>