<?
class Zend_View_Helper_Vacio extends Zend_View_Helper_Abstract
{
	/**
	*Rafael German
	comprueba si el objecto esta vacio para evitar errores al querer ver la variable de un objecto vacio
	*public
	*@param $list object 
	*@param $field string 
	*@return value
	*/
    public function vacio($list, $field,$indice=0){
    	if(is_array($list))
    		$variable=utf8_decode((empty($list[$indice])?'':$list[$indice][$field]));
    	else
       		$variable=utf8_decode((empty($list)? '' : $list->$field));
    	return $this->view->checkdate($variable);
    }
}
?>