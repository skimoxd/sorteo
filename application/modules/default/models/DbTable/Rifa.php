<?
include_once('GeneralModel.php');
class Default_Model_DbTable_Rifa extends Default_Model_DbTable_GeneralModel
{
	protected $_name= 'sorteo';
	protected $_primary= 'id_sorteo';
	protected $vista='sorteo';
	public function guardar($bind, $id=null){		
		$ret = parent::guardar($bind, $id);	
		return $ret;
	}
	public function cambiarestado($id){
		return parent::cambiarestado($id);
	}
	public function busqueda($estado=1,$id=null,$ret=false){
		return parent::busqueda($estado,$id);
	}
	public function obtenerActivos(){
		$select=parent::busqueda(1,null,true);
		$select->where("estado=1 and fecha_inicio<= CURDATE() and CURDATE() <=fecha_final");
		return $this->fetchAll($select);
	}
	public function obtenerRifasActivas(){
		$select=$this->select()
			->setIntegrityCheck(false)
			->from('v_rifa_activa');
		return $this->fetchAll($select);
	}
	public function obtenerboletos($id_sorteo){

    	$select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from("sorteo")
            ->where('id_sorteo = ? ',$id_sorteo);
       	$limite=$this->fetchRow($select)->limite;
        $select=$this->select();
        $select->setIntegrityCheck(false);
        $select->from("boleto")
            ->where('id_sorteo = ? ',$id_sorteo)
            ->where('estado!=0');
       	$boletos=$this->fetchAll($select);
        $boletosdisponibles=array();
        for($i=0;$i<$limite;$i++)
        {	
        	$boletosdisponibles[$i]["num_boleto"]=$i+1;
        	$boletosdisponibles[$i]["estado"]=0;
        	foreach($boletos as $b)
        	{
        		if($b->num_boleto==($i+1))
        			$boletosdisponibles[$i]["estado"]=$b->estado;
        	}
        }
        return $boletosdisponibles;
    }

    public function obtenerapi(){
    	$select=$this->select()
			->setIntegrityCheck(false)
			->from('webapi');
		return $this->fetchRow($select);
    }
}?>