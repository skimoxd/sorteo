<?
class Default_Model_DbTable_GeneralModel extends Zend_Db_Table_Abstract{
	protected $_sequence = true;
	protected $id_sucursal='';
	protected $tiposucursal='';
	protected $ranking='';
	protected $dsucursal=false;
	protected $sstipo=false;
	protected $id_usuario='';
	protected $id_empleado='';
	//protected $permiso='';
	//protected $permisos=array();
	public function __construct(){
		parent::__construct();
		$authSession = Zend_Auth::getInstance();
		$authSession->setStorage(new Zend_Auth_Storage_Session('rifa'));
		if($authSession->hasIdentity()){
			$identity=$authSession->getIdentity();
			if(gettype($identity)=='object'){
				foreach($identity as $attribute => $value){
					if(isset($this->$attribute)){
						$this->$attribute=$value;
					}
				}
				/*$permisos=explode("|",$this->permiso);
		        $datos=array();
		        foreach($permisos as $p){
		            $permiso=explode(",",$p);
		            $datos[$permiso[0]]=$permiso[1];
		        }
		        $this->permisos=$datos;*/
			}
		}
	}
	protected function guardar($data,$id){
		foreach($data as $i=>$v){
			if(!is_array($v) && !is_object($v))
				if(!empty($v))
					$data[$i]=utf8_encode($v);
		}
		if(empty( $id ))
			$row = $this->createRow();
		else
			$row =$this->busqueda(false,$id);
		if($this->dsucursal)
			$data["id_sucursal"]=$this->id_sucursal;
		if(is_null($row))
		 	return -1;
		 $row->setFromArray( $data);
		 return $row->save();
		
	}
	protected function busqueda($estado=1,$id=null,$ret=false){
		if(is_null($id)){
			$select = $this->select()
				->setIntegrityCheck(false)
				->from($this->vista);
			if($estado!==false)
				$select->where('estado = ?' , $estado);
			if($this->dsucursal and $this->tiposucursal!=4)
				$select->where('id_sucursal = ?' ,$this->id_sucursal);
			if($this->sstipo and $this->tiposucursal!=4)
				$select->where('FIND_IN_SET(?, unidad_negocios) ' , intval($this->tiposucursal));
		}else{
			$select = $this->select()
				->setIntegrityCheck(false)
				->from($this->vista);
			$select->where((is_array($this->_primary)?$this->_primary[1]:$this->_primary).' = ?',$id);
			if($estado!==false)
				$select->where('estado = ?' , $estado);
		}
		if($ret)
			return $select;
		else
			if(is_null($id))
				return $this->fetchAll($select);
			else
				return $this->fetchRow($select);
	}
	protected function cambiarestado($id){
		$where = $this->getAdapter()->quoteInto($this->_primary.' = ?',$id);
		return $this->update(array("estado" =>new Zend_Db_Expr("if(estado=1,0,1)"),"desactivado_desde"=> new Zend_Db_Expr("now()")),$where);
	}

}?>