<?
include_once('GeneralModel.php');
class Default_Model_DbTable_Abono extends Default_Model_DbTable_GeneralModel
{
	protected $_name= 'abonos';
	protected $_primary= 'id_abono';
	protected $vista='v_abonos';
	public function guardar($bind, $id=null){		
		$ret = parent::guardar($bind, $id);	
		return $ret;
	}
	public function cambiarestado($id){
		return parent::cambiarestado($id);
	}
	public function busqueda($estado=1,$id=null,$ret=false){
		return parent::busqueda($estado,$id);
	}
	public function comprobarimporte($id,$importe,$abono=null){
		$select=$this->select();
		$select->setIntegrityCheck(false);
        $select->from(array("b"=>"boleto"))
        		->joinleft(array("a"=>"abonos"),"b.id_boleto=a.id_boleto  and a.estado=1",array())
        		->join(array("s"=>"sorteo"),"s.id_sorteo=b.id_sorteo",array());
        $select->columns(array("estatus"=>"if(ifnull(sum(a.importe),0)+".$importe."<=costo,0,1)"))
            ->where('b.id_boleto = ? ',$id);
       	if(!is_null($abono))
       		$select->where('a.id_abono != ? ',$abono);
       	return $this->fetchRow($select)->estatus;
	}
    public function obtenerAbono($fecha1,$fecha2){
		$this->vista="v_abonos_cuenta";
		$select=parent::busqueda(false,null,true);
		if(!empty($fecha2))
			$select->where("fecha < STR_TO_DATE(?,'%d/%m/%Y')",$fecha2);
		if(!empty($fecha1))
			$select->where("fecha > STR_TO_DATE(?,'%d/%m/%Y')",$fecha1);
		return $this->fetchAll($select);
    }
    public function ObtenerBoleto($id_boleto){
    	$select=$this->select();
        $select->setIntegrityCheck(false);
        $select->from("boleto")
       
            ->where('id_boleto = ? ',$id_boleto);
        return $this->fetchRow($select);
    }
	public function Actualizarboleto($id_boleto){
		$estado=1;
		$select=$this->select();
		$select->setIntegrityCheck(false);
        $select->from(array("b"=>"boleto"))
        		->joinleft(array("a"=>"abonos"),"b.id_boleto=a.id_boleto and a.estado=1" ,array())
        		->join(array("s"=>"sorteo"),"s.id_sorteo=b.id_sorteo",array());
        $select->columns(array("importe"=>"ifnull(sum(a.importe),0)","costo"=>"s.costo"))
            ->where('b.id_boleto = ? ',$id_boleto);
        $registro=$this->fetchRow($select);
       	$importe=$registro->importe;
       	$costo=$registro->costo;
        if($importe==0)
        	$estado=1;
        else{
        	if($costo>$importe)
        		$estado=2;
        	else
        		$estado=3;
        }
        $table = new Zend_Db_Table('boleto');
		$datos = array(
		   'estado' => $estado
		);
		$where = $this->getAdapter()->quoteInto('id_boleto = ?', $id_boleto);
		$table->update($datos,$where);


	}
	public function desactivar($id){
		$data=array(
			"estado"=>0,
			"desactivado_desde"=> new Zend_Db_Expr("now()")
		);
		$where = $this->getAdapter()->quoteInto('id_abono = ?',$id);
        return $this->update($data,$where);
	}
	public function activar($id){
		$data=array(
			"estado"=>1
		);
		$where = $this->getAdapter()->quoteInto('id_abono = ?',$id);
        return $this->update($data,$where);
	}
	public function obtenerimporte($id){
		$select=$this->select();
		$select->setIntegrityCheck(false);
        $select->from(array("b"=>"boleto"))
        		->joinleft(array("a"=>"abonos"),"b.id_boleto=a.id_boleto and a.estado=1",array())
        		->join(array("s"=>"sorteo"),"s.id_sorteo=b.id_sorteo",array());
        $select->columns(array("importe"=>"ifnull(sum(a.importe),0)"))
            ->where('a.id_abono = ? ',$id);
        return $this->fetchRow($select)->importe;
	}
	public function obtenerboletosapartados($id_sorteo,$id_boleto){

        $select=$this->select();
        $select->setIntegrityCheck(false);
        $select->from("boleto")
       
            ->where('id_sorteo = ? ',$id_sorteo)
            ->where("estado in (1,2) or id_boleto = ?",$id_boleto);
        return $this->fetchAll($select);
       
    }
    public function guardararchivo($ruta,$id_abono)
	{
		$datos = array(
		   'archivo' => $ruta
		);
		$where = $this->getAdapter()->quoteInto('id_abono = ?', $id_abono);
		$this->update($datos,$where);
	}

	public function ProcesoEstado($boleto){
		$select=$this->select();
        $select->setIntegrityCheck(false);
        $select->from("abonos")
       		->columns(array("proceso"=>"case when count(*)>=1 then '1' else '0' end "))
            ->where('id_boleto = ?',$boleto)
            ->where('archivo<>\'\'');
        $proceso=$this->fetchRow($select)->proceso;
        if($proceso==1)
        {
        	$table = new Zend_Db_Table('boleto');
        	$datos = array(
			   'estado' => 4
			);
        	$where = $this->getAdapter()->quoteInto('id_boleto = ?',$boleto);
        	$table->update($datos,$where);
        }

	}



}?>