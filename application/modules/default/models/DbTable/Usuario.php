<?
include_once('GeneralModel.php');
class Default_Model_DbTable_Usuario extends Default_Model_DbTable_GeneralModel
{
	protected $_name= 'usuario';
	protected $_primary= 'id_usuario';
	protected $vista='usuario';
	public function guardar($bind, $id=null){		
		$ret = parent::guardar($bind, $id);	
		return $ret;
	}
	public function obtenerDatos($usuario,$contrasena){
		$select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from('usuario')
            ->where('usuario= ?',$usuario)
            ->where('contrasena = ?',$contrasena)
            ->where('estado=1');
        return $this->fetchRow($select);
	}
	public function comprobarUsuario($id,$usuario){
		$select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from($this->_name)
            ->where('usuario= ?',$usuario)
            ->where('id_usuario != ?',$id);
        return $this->fetchRow($select);
	}
	public function comprobarEmail($id,$email){
		$select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from($this->_name)
            ->where('correo= ?',$email)
            ->where('id_usuario != ?',$id);
        return $this->fetchRow($select);
	}
	public function cambiarestado($id){
		return parent::cambiarestado($id);
	}
	public function busqueda($estado=1,$id=null,$ret=false){
		return parent::busqueda($estado,$id);
	}

}?>