<?
include_once('GeneralModel.php');
class Default_Model_DbTable_Cuenta extends Default_Model_DbTable_GeneralModel
{
	protected $_name= 'cuenta';
	protected $_primary= 'id_cuenta';
	protected $vista='cuenta';
	public function guardar($bind, $id=null){		
		$ret = parent::guardar($bind, $id);	
		return $ret;
	}
	public function cambiarestado($id){
		return parent::cambiarestado($id);
	}
	public function busqueda($estado=1,$id=null,$ret=false){
		return parent::busqueda($estado,$id);
	}

}?>