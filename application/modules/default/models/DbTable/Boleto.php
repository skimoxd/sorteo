<?
include_once('GeneralModel.php');
class Default_Model_DbTable_Boleto extends Default_Model_DbTable_GeneralModel
{
	protected $_name= 'boleto';
	protected $_primary= 'id_boleto';
	protected $vista='v_boletos';
	public function guardar($bind, $id=null){		
		$ret = parent::guardar($bind, $id);	
		return $ret;
	}
	public function busqueda($estado=1,$id=null,$ret=false){ 
		$ret=parent::busqueda($estado,$id,true);
        $ret->where('estatus_rifa = 1 and final_rifa>=NOW()');
        if(is_null($id))
            return $this->fetchAll($ret);
        else
            return $this->fetchRow($ret);
	}
	public function obtenerrifa($unico=false,$id=null){
		$select=$this->select()
			->setIntegrityCheck(false)
			->from('v_rifa_activa');
        if($unico){
            $select->where("id_sorteo = ?",$id);
            return $this->fetchRow($select);   
        }
        else
		    return $this->fetchAll($select);
	}
	public function ComprobarBoleto($id,$boleto,$sorteo){
		$select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from($this->_name)
            ->where('num_boleto = ? ',$boleto)
            ->where('id_sorteo = ? ',$sorteo)
            ->where('id_boleto != ?',$id)
            ->where('estado!=0');
        return $this->fetchRow($select);
	}
	public function obtenerCantidad($sorteo){
		$select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from("sorteo")
            ->where('id_sorteo = ? ',$sorteo);
        return $this->fetchRow($select)->limite;
	}
    public function obtenerLadas(){
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from("ladas");
        return $this->fetchAll($select);
    }
    public function obtenernumlada($lada){
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from("ladas")
                ->where("id_lada = ?",$lada);
        return $this->fetchRow($select)->num;
    }
    public function obtenerBoletos($id_sorteo){
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from("v_boleto_reporte")
            ->where('id_sorteo = ? ',$id_sorteo);
        return $this->fetchAll($select);
    }
	public function borraroportunidades($id_boleto){
        $table = new Zend_Db_Table('oportunidades');
        $where = $this->getAdapter()->quoteInto('id_boleto = ?',$id_boleto);
        return $table->delete($where);
    }
    public function guardaroportunidad($datos){ 
        $table = new Zend_Db_Table('oportunidades');
        return $table->insert($datos);
    }
    public function obtenerboletosdisponibles($id_sorteo,$boleto=null){

    	$select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from("sorteo")
            ->where('id_sorteo = ? ',$id_sorteo);
       	$limite=$this->fetchRow($select)->limite;
        $select=$this->select();
        $select->setIntegrityCheck(false);
        $select->from("boleto");
        $select->columns(array("boletos"=>"group_concat(num_boleto separator ',')"))
            ->where('id_sorteo = ? ',$id_sorteo)
            ->where('estado!=0');
        if(!is_null($boleto))
             $select->where('num_boleto != ? ',$boleto);
       	$boletos=$this->fetchRow($select)->boletos;
        $boletosArray=explode(",", $boletos);
        $boletosdisponibles=array();
        for($i=0;$i<$limite;$i++)
        {	
        	if(!in_array($i+1,$boletosArray))
        		$boletosdisponibles[] = $i+1;
        }
        return $boletosdisponibles;
    }

    public function obtenerboletosapartados($id_sorteo){

        $select=$this->select();
        $select->setIntegrityCheck(false);
        $select->from("boleto")
       
            ->where('id_sorteo = ? ',$id_sorteo)
            ->where("estado in (1,2)");
        return $this->fetchAll($select);
       
    }

    public function cambiarestado($id){
		return parent::cambiarestado($id);
	}
	public function guardararchivo($ruta,$id_boleto)
	{
		$datos = array(
		   'archivo' => $ruta
		);
		$where = $this->getAdapter()->quoteInto('id_boleto = ?', $id_boleto);
		$this->update($datos,$where);
	}
    public function obtenerapi(){
        $select=$this->select()
            ->setIntegrityCheck(false)
            ->from('webapi');
        return $this->fetchRow($select);
    }
}
?>