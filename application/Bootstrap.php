<?
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    public function _initAutoload()
	{
		$modules = array(
			'Default'
			);

		foreach($modules as $module)
		{
			$autoloader = new Zend_Application_Module_Autoloader(array(
				'namespace'=>ucfirst($module),
				'basePath'=>APPLICATION_PATH . '/modules/' . strtolower($module),
				));
		}

		return $autoloader;
	}

	protected function _initRegistry(){

    $this->bootstrap('db');
    $db = $this->getResource('db');

    $db->setFetchMode(Zend_Db::FETCH_OBJ);

    Zend_Registry::set('db', $db);
	}
   protected function _initModules()
    {
        $front = Zend_Controller_Front::getInstance();
        $front->addModuleDirectory('../application/modules');
    }
}