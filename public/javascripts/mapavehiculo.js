var mapa={
  map:null,
  vehiculos:[],
  enable_tab:true,
  vectorSource:null,
  info:null,
  fvehiculos:[],
  jTarget:null,
  error:0,
  style: {
      icon: new ol.style.Style({
        image: new ol.style.Icon({
          anchor: [0.5, 0.5],
          src: "/imagenes/motocicleta1.png"
        })
      })
    },
  iniciar:function(){
    mapa.info = $('#info');
    mapa.info.tooltip({
      animation: false,
      trigger: 'manual'
    });
    mapa.vectorSource = new ol.source.Vector();
    var vectorLayer = new ol.layer.Vector({
      source: mapa.vectorSource
    });
    mapa.map = new ol.Map({
      layers: [new ol.layer.Tile({ source: new ol.source.OSM() }), vectorLayer],
      target: document.getElementById('map'),
      view: new ol.View({
        center: ol.proj.transform([-110.981230,29.111286] , 'EPSG:4326', 'EPSG:3857'),
        zoom: 15
      })
    });
    $(".datepicker").datepicker();
    $('#start, #end').timepicker({
        'showDuration': true,
        'timeFormat': 'g:i a'
    });
    $("#miformulario").validator();
    $('#miformulario').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) 
          general.alerta("Checa los campos",4);
        else 
          mapa.trazar();
        return false;
    });
    $("#trazar").on('click',function(){
      $("#miformulario").submit();
    });
    $("#start").timepicker('option', 'maxTime', '11:00pm');
    $("#end").timepicker('option', 'minTime', '1:00am');
    $("#start").on("change",function(){
      mapa.movertiempo("end");
    });
    $("#end").on("change",function(){
      mapa.movertiempo("start");
    });
  },
  movertiempo:function(idtime){
    var startdate=$("#start").timepicker('getTime');
    var enddate=$("#end").timepicker('getTime');
    var horasDif = enddate.getTime() - startdate.getTime();
    var horas = horasDif/(1000 * 60 * 60 );
    if(horas<=0){
      if(idtime=="end"){
        enddate.setTime(startdate.getTime()+3600000);
        $("#"+idtime).timepicker("setTime",enddate);
      }
      if(idtime=="start"){
        startdate.setTime(enddate.getTime()-3600000);
        $("#"+idtime).timepicker("setTime",startdate);
      }
    }
  },
  trazar:function(){
    var form=$("#miformulario").serializeObject();
    var funsuccess=function(data){
      if(data["tipo"]==1){
        if(data["mensaje"].length == 0)
          general.alerta("No hay coordenadas para trazar la ruta",4);
        else
        {
          var coordenadas=data["mensaje"];
          mapa.ruta(coordenadas);
        }
      }
    }
    var funcomplete=function(){
      
    }
    var funbefore=function(){
    }
    var funerror=function(){

    }
    general.cargarajax('/mapa/trazar',{datos:form},funsuccess,funbefore,funcomplete,funerror,"json");
  },
  ruta:function(coordenadas){
    mapa.vectorSource.clear();
      if(coordenadas.length>0){
        var secondpoint=null;
        var firstpoint=1;
        var feature = new ol.Feature({
          type: 'place',
          geometry: new ol.geom.Point(ol.proj.transform([-110.981230,29.111286], 'EPSG:4326',     
                'EPSG:3857'))
        });
        var style=new ol.style.Style({
          image: new ol.style.Icon({
            anchor: [0.5, 0.5],
            src: "/imagenes/inicio.png"
          })
        });
        feature.setStyle(style);
        mapa.vectorSource.addFeature(feature);
        for(var x=1;x<coordenadas.length;x++){
          firstpoint=[coordenadas[(x-1)]["lng"], coordenadas[(x-1)]["lat"]];
          secondpoint=[coordenadas[x]["lng"], coordenadas[x]["lat"]];
          var feature = new ol.Feature({
            type: 'place',
            geometry: new ol.geom.Point(ol.proj.transform([coordenadas[x]["lng"], coordenadas[x]["lat"]], 'EPSG:4326',     
                  'EPSG:3857'))
          });
          feature.setStyle(mapa.style.icon);
          mapa.vectorSource.addFeature(feature);
          fetch('//router.project-osrm.org/route/v1/driving/' + firstpoint.join()+ ';' + secondpoint.join()).then(function(r) { 
            return r.json();
          }).then(function(json) {
            var polyline=json.routes[0].geometry;
            var route = new ol.format.Polyline({
              factor: 1e5
            }).readGeometry(polyline, {
              dataProjection: 'EPSG:4326',
              featureProjection: 'EPSG:3857'
            });
            var feature = new ol.Feature({
              type: 'route',
              geometry: route
            });
            feature.setStyle(mapa.style.route);
            mapa.vectorSource.addFeature(feature);
          }).catch(function(){
            if(mapa.error==0){
              mapa.error=1;
              setTimeout(function(){
                general.alerta("No se puedo mostrar la ruta, intentelo mas tarde",4);
                mapa.vectorSource.clear();
              },500);
            }
          });
          
        }
      }
      mapa.error=0;
  }
};
      
      
