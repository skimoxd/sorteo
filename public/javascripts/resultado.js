var resultado={
	GastosMir:[],
	NominaMir:[],
	IngresosMir:[],
	GastosTLT:[],
	NominaTLT:[],
	CompraMir:[],
	CompraTLT:[],
	CompraTinker:[],
	IngresosTLT:[],
	GastosTinker:[],
	NominaTinker:[],
	IngresosTinker:[],
	GastosMir2:[],
	NominaMir2:[],
	IngresosMir2:[],
	GastosTLT2:[],
	NominaTLT2:[],
	IngresosTLT2:[],
	GastosTinker2:[],
	NominaTinker2:[],
	IngresosTinker2:[],
	CompraMir2:[],
	CompraTLT2:[],
	CompraTinker2:[],
	GastosDesglozados:[],
	GastosDesglozadosTLT:[],
	GastosDesglozadosTINKER:[],
	GastosDesglozadosCEIMSA:[],
	//var $total:0,
	iniciar:function(){
		var adata=[];
		var adata2=[];
		var adata3=[];
		$SumaGasto = 0;
		$SumaNomina = 0;
		$SumaIngresos = 0;
		$SumaCompra = 0;
		//Tabla MIR
		resultado.GastosMir.forEach(function(value){
			if (value.id_sucursal == 1) {
				$SumaGasto +=  Math.round((parseFloat(value.Gastos))/3);
			}else
			$SumaGasto += parseFloat(value.Gastos);

		}); 
		resultado.NominaMir.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaNomina +=  Math.round((parseFloat(value.Nomina))/3);
			}else
			$SumaNomina += parseFloat(value.Nomina);
		});
		resultado.IngresosMir.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaIngresos +=  Math.round((parseFloat(value.Ingresos))/3);
			}
			$SumaIngresos += parseFloat(value.Ingresos);
		});
		resultado.CompraMir.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaCompra +=  Math.round((parseFloat(value.Compras))/3);
			}
			$SumaCompra += parseFloat(value.Compras);
		});
		resultado.cargarchart("SMIR",$SumaIngresos,$SumaGasto,$SumaNomina,$SumaCompra);
		$SumaGasto = 0;
		$SumaNomina = 0;
		$SumaIngresos = 0;
		$SumaCompra = 0;

		//Tabla Tinker
		resultado.GastosTinker.forEach(function(value){
			if (value.id_sucursal == 1) {
				$SumaGasto +=  Math.round((parseFloat(value.Gastos))/3);
			}else
			$SumaGasto += parseFloat(value.Gastos);

		}); 
		resultado.NominaTinker.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaNomina +=  Math.round((parseFloat(value.Nomina))/3);
			}else
			$SumaNomina += parseFloat(value.Nomina);
		});
		resultado.IngresosTinker.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaIngresos +=  Math.round((parseFloat(value.Ingresos))/3);
			}
			$SumaIngresos += parseFloat(value.Ingresos);
		});
		resultado.CompraTinker.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaCompra +=  Math.round((parseFloat(value.Compras))/3);
			}
			$SumaCompra += parseFloat(value.Compras);
		});
		resultado.cargarchart("STINKER",$SumaIngresos,$SumaGasto,$SumaNomina,$SumaCompra);
		$SumaGasto = 0;
		$SumaNomina = 0;
		$SumaIngresos = 0;
		$SumaCompra = 0;

		//Tabla TLT
		resultado.GastosTLT.forEach(function(value){
			if (value.id_sucursal == 1) {
				$SumaGasto +=  Math.round((parseFloat(value.Gastos))/3);
			}else
			$SumaGasto += parseFloat(value.Gastos);

		}); 
		resultado.NominaTLT.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaNomina +=  Math.round((parseFloat(value.Nomina))/3);
			}else
			$SumaNomina += parseFloat(value.Nomina);
		});
		resultado.IngresosTLT.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaIngresos +=  Math.round((parseFloat(value.Ingresos))/3);
			}
			$SumaIngresos += parseFloat(value.Ingresos);
		});
		resultado.CompraTLT.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaCompra +=  Math.round((parseFloat(value.Compras))/3);
			}
			$SumaCompra += parseFloat(value.Compras);
		});
		resultado.cargarchart("STLT",$SumaIngresos,$SumaGasto,$SumaNomina,$SumaCompra);

		//Tabla 2

		var adata=[];
		var adata2=[];
		var adata3=[];
		$SumaGasto = 0;
		$SumaNomina = 0;
		$SumaIngresos = 0;
		$SumaCompra = 0;
		//Tabla MIR
		resultado.GastosMir2.forEach(function(value){
			if (value.id_sucursal == 1) {
				$SumaGasto +=  Math.round((parseFloat(value.Gastos))/3);
			}else
			$SumaGasto += parseFloat(value.Gastos);

		}); 
		resultado.NominaMir2.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaNomina +=  Math.round((parseFloat(value.Nomina))/3);
			}else
			$SumaNomina += parseFloat(value.Nomina);
		});
		resultado.IngresosMir2.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaIngresos +=  Math.round((parseFloat(value.Ingresos))/3);
			}
			$SumaIngresos += parseFloat(value.Ingresos);
		});
		resultado.CompraMir2.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaCompra +=  Math.round((parseFloat(value.Compras))/3);
			}
			$SumaCompra += parseFloat(value.Compras);
		});
		resultado.cargarchart("MMIR",$SumaIngresos,$SumaGasto,$SumaNomina,$SumaCompra);
		$SumaGasto = 0;
		$SumaNomina = 0;
		$SumaIngresos = 0;
		$SumaCompra = 0;

		//Tabla Tinker
		resultado.GastosTinker2.forEach(function(value){
			if (value.id_sucursal == 1) {
				$SumaGasto +=  Math.round((parseFloat(value.Gastos))/3);
			}else
			$SumaGasto += parseFloat(value.Gastos);

		}); 
		resultado.NominaTinker2.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaNomina +=  Math.round((parseFloat(value.Nomina))/3);
			}else
			$SumaNomina += parseFloat(value.Nomina);
		});
		resultado.IngresosTinker2.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaIngresos +=  Math.round((parseFloat(value.Ingresos))/3);
			}
			$SumaIngresos += parseFloat(value.Ingresos);
		});
		resultado.CompraTinker2.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaCompra +=  Math.round((parseFloat(value.Compras))/3);
			}
			$SumaCompra += parseFloat(value.Compras);
		});
		resultado.cargarchart("MTINKER",$SumaIngresos,$SumaGasto,$SumaNomina,$SumaCompra);
		$SumaGasto = 0;
		$SumaNomina = 0;
		$SumaIngresos = 0;
		$SumaCompra = 0;

		//Tabla TLT
		resultado.GastosTLT2.forEach(function(value){
			if (value.id_sucursal == 1) {
				$SumaGasto +=  Math.round((parseFloat(value.Gastos))/3);
			}else
			$SumaGasto += parseFloat(value.Gastos);

		}); 
		resultado.NominaTLT2.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaNomina +=  Math.round((parseFloat(value.Nomina))/3);
			}else
			$SumaNomina += parseFloat(value.Nomina);
		});
		resultado.IngresosTLT2.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaIngresos +=  Math.round((parseFloat(value.Ingresos))/3);
			}
			$SumaIngresos += parseFloat(value.Ingresos);
		});
		resultado.CompraTLT2.forEach(function(value){
			if(value.id_sucursal == 1){
				$SumaCompra +=  Math.round((parseFloat(value.Compras))/3);
			}
			$SumaCompra += parseFloat(value.Compras);
		});
		resultado.cargarchart("MTLT",$SumaIngresos,$SumaGasto,$SumaNomina,$SumaCompra);

		//Tabla2
		//Gastos MIR
		var alabels=[];
		var adata=[];
		var arreglo=[]
		resultado.GastosDesglozados.forEach(function(value){
			if(arreglo[value.descripcion] != undefined )
				arreglo[value.descripcion]+=parseFloat(value.total);
			else
				arreglo[value.descripcion]=parseFloat(value.total);
		});
		Object.keys(arreglo).forEach(function (key) {
			alabels.push(key);
			adata.push(arreglo[key]);
		});
		resultado.cargarchart3("DGMIR",alabels,adata);
		//Gastos TLT
		var alabels=[];
		var adata=[];
		var arreglo=[]
		resultado.GastosDesglozadosTLT.forEach(function(value){
			if(arreglo[value.descripcion] != undefined )
				arreglo[value.descripcion]+=parseFloat(value.total);
			else
				arreglo[value.descripcion]=parseFloat(value.total);
		});
		Object.keys(arreglo).forEach(function (key) {
			alabels.push(key);
			adata.push(arreglo[key]);
		});
		resultado.cargarchart3("DGTLT",alabels,adata);
		//Gastos Tinker
		var alabels=[];
		var adata=[];
		var arreglo=[]
		resultado.GastosDesglozadosTINKER.forEach(function(value){
			if(arreglo[value.descripcion] != undefined )
				arreglo[value.descripcion]+=parseFloat(value.total);
			else
				arreglo[value.descripcion]=parseFloat(value.total);
		});
		Object.keys(arreglo).forEach(function (key) {
			alabels.push(key);
			adata.push(arreglo[key]);
		});
		resultado.cargarchart3("DGTIN",alabels,adata);


		
	/*
		var alabels=[];
		var adata=[];
		resultado.GastosMir.forEach(function(value){
			alabels.push(value.nombre);
			adata.push(value.num_docs);
		});
		resultado.cargarchart("reps_docs",alabels,adata,"# documentos completados");
		alabels=[];
		adata=[];
		resultado.IngresosMir.forEach(function(value){
			alabels.push(value.nombre);
			adata.push(value.gasto);
		});
		resultado.cargarchart("gastos_emp",alabels,adata,"gastos");
		alabels=[];
		adata=[];
		resultado.NominaMir.forEach(function(value){
			alabels.push("# "+value.num_vehiculo);
			adata.push(value.gasto);
		});
		resultado.cargarchart("gastos_veh",alabels,adata,"gastos");*/
	},
	cargarchart:function(id_canva,$SumaIngresos,$SumaGasto,$SumaNomina,$SumaCompra){
		var ctx = document.getElementById(id_canva).getContext('2d');
		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["Ingresos", "Nomina", "Gastos de operacion","Compras"],
		        datasets: [{
		            label: 'Reporte Semanal',
		            data: [$SumaIngresos,$SumaNomina , $SumaGasto, $SumaCompra],
		            backgroundColor: [
		                'rgba(255, 99, 132, 0.2)',
		                'rgba(54, 162, 235, 0.2)',
		                'rgba(255, 206, 86, 0.2)',
		                'rgba(75, 192, 192, 0.2)',
		                'rgba(153, 102, 255, 0.2)',
		                'rgba(255, 159, 64, 0.2)'
		            ],
		            borderColor: [
		                'rgba(255,99,132,1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)',
		                'rgba(153, 102, 255, 1)',
		                'rgba(255, 159, 64, 1)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		});
	}, //Termina primera grafica
	cargarchart2:function(){
		var ctx = document.getElementById("mensual").getContext('2d');
		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["Ingresos", "Nomina", "Gastos de operacion"],
		        datasets: [{
		            label: 'Reporte Mensual',
		            data: [8000000, 3854005, 1530432],
		            backgroundColor: [
		                'rgba(255, 99, 132, 0.2)',
		                'rgba(54, 162, 235, 0.2)',
		                'rgba(255, 206, 86, 0.2)',
		                'rgba(75, 192, 192, 0.2)',
		                'rgba(153, 102, 255, 0.2)',
		                'rgba(255, 159, 64, 0.2)'
		            ],
		            borderColor: [
		                'rgba(255,99,132,1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)',
		                'rgba(153, 102, 255, 1)',
		                'rgba(255, 159, 64, 1)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		});
	}, //Termina segunda grafica
	cargarchart3:function(id_canva,alabels,adata){
		var ctx = document.getElementById(id_canva).getContext('2d');
		var myChart = new Chart(ctx, {
		    type: 'polarArea',
		    data: {
		        labels: alabels,
		        datasets: [{
		            label: 'Reporte Semanal',
		            data: adata,
		            backgroundColor: [
		                'rgba(255, 99, 132, 0.2)',
		                'rgba(54, 162, 235, 0.2)',
		                'rgba(255, 206, 86, 0.2)',
		                'rgba(75, 192, 192, 0.2)',
		                'rgba(153, 102, 255, 0.2)',
		                'rgba(255, 159, 64, 0.2)'
		            ],
		            borderColor: [
		                'rgba(255,99,132,1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)',
		                'rgba(153, 102, 255, 1)',
		                'rgba(255, 159, 64, 1)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		});
	}
}