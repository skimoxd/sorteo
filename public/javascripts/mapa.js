var mapa={
  map:null,
  vehiculos:[],
  enable_tab:true,
  vectorSource:null,
  info:null,
  fvehiculos:[],
  jTarget:null,
  style: {
      icon: new ol.style.Style({
        image: new ol.style.Icon({
          anchor: [0.5, 0.5],
          src: "/imagenes/motocicleta1.png"
        })
      })
    },
  iniciar:function(){
    mapa.info = $('#info');
    mapa.info.tooltip({
      animation: false,
      trigger: 'manual'
    });
    mapa.vectorSource = new ol.source.Vector();
    var vectorLayer = new ol.layer.Vector({
      source: mapa.vectorSource
    });
    mapa.map = new ol.Map({
      layers: [new ol.layer.Tile({ source: new ol.source.OSM() }), vectorLayer],
      target: document.getElementById('map'),
      view: new ol.View({
        center: ol.proj.transform([-110.981230,29.111286] , 'EPSG:4326', 'EPSG:3857'),
        zoom: 15
      })
    });
    mapa.vehiculos.forEach(function(value){
       var feature = new ol.Feature({
          type: 'place',
          geometry: new ol.geom.Point(ol.proj.transform([parseFloat(value.lng),parseFloat(value.lat)], 'EPSG:4326',     
                'EPSG:3857'))
        });
        var style=new ol.style.Style({
          image: new ol.style.Icon({
            anchor: [0.5, 0.5],
            src: value.icono
          })
        });
        feature.setStyle(style);
        feature.attributes = {
            name: value.placa
        };
        mapa.fvehiculos[value.idvehiculo]=feature;
        mapa.vectorSource.addFeature(feature);
    });
    /*mapa.map.on('pointermove', function(evt) {
      if (evt.dragging) {
        mapa.info.tooltip('hide');
        return;
      }
      mapa.displayFeatureInfo(mapa.map.getEventPixel(evt.originalEvent));
    });*/
    var target = mapa.map.getTarget();
    mapa.jTarget = typeof target === "string" ? $("#" + target) : $(target);
    mapa.map.on('pointermove', function(evt) {
      var pixel = mapa.map.getEventPixel(evt.originalEvent);
      var hit = mapa.map.forEachFeatureAtPixel(pixel, function (feature, layer) {
          return true;
      });
      if (hit) {
          mapa.jTarget.css("cursor", "pointer");
      } else {
          mapa.jTarget.css("cursor", "");
      }
    });
    mapa.map.on('click', function(evt) {
      mapa.displayFeatureInfo(evt.pixel);
    });
    /*mapa.map = new OpenLayers.Map("map");
    var mapnik         = new OpenLayers.Layer.OSM();
    var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
    var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
    var position       = new OpenLayers.LonLat(-110.981230,29.111286).transform( fromProjection, toProjection);
    var zoom           = 15; 
    mapa.map.addLayer(mapnik);
    mapa.markers = new OpenLayers.Layer.Markers( "Markers" );
    mapa.map.addLayer(mapa.markers);
   
    var size = new OpenLayers.Size(21,25);
    var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
    mapa.vehiculos.forEach(function(value){
      
      var icon = new OpenLayers.Icon('/imagenes/motocicleta.png', size, offset);
       var lonLat = new OpenLayers.LonLat( value.lng,value.lat ).transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            mapa.map.getProjectionObject() // to Spherical Mercator Projection
          );
      mapa.markers.addMarker(new OpenLayers.Marker(lonLat,icon));
      
    });

    var points = new Array(
        new OpenLayers.Geometry.Point(-110.9679011,29.1418963),
        new OpenLayers.Geometry.Point(-110.9683764,29.1425534)
    );

    var myLine = new OpenLayers.Geometry.LineString(points).transform("EPSG:4326", mapa.map.getProjectionObject());
    var myLineStyle = {strokeColor:"#0500bd", strokeWidth:3};
    var myFeature = new OpenLayers.Feature.Vector(myLine, {}, myLineStyle);
    var myVector = new OpenLayers.Layer.Vector("line test");
    myVector.addFeatures([myFeature]);
    mapa.map.addLayers([myVector]);
    mapa.map.setCenter(position, zoom );*/
    setInterval(function(){ mapa.actualizar() }, 10000);
  },
  actualizar:function(){
    var funsuccess=function(data){
      if(data["tipo"]==1){
        mapa.vectorSource.clear();
        data["datos"].forEach(function(value){
          var feature = new ol.Feature({
            type: 'place',
            geometry: new ol.geom.Point(ol.proj.transform([parseFloat(value.lng),parseFloat(value.lat)], 'EPSG:4326',     
                  'EPSG:3857'))
          });
          var style=new ol.style.Style({
            image: new ol.style.Icon({
              anchor: [0.5, 0.5],
              src: value.icono
            })
          });
          feature.setStyle(style);
          feature.attributes = {
            name: value.num_vehiculo + " - " + value.placa
          };
          mapa.vectorSource.addFeature(feature);
        });
      }
      if(data["tipo"]==4){
        setTimeout(function(){ window.location.reload(true); },1000);
      }
    }
    var funcomplete=function(){

    }
    var funbefore=function(){

    }
    var funerror=function(){

    }
    general.cargarajax('/index/obtenervehiculos',{},funsuccess,funbefore,funcomplete,funerror,"json");
  },
  displayFeatureInfo:function(pixel) {
    mapa.info.css({
      left: pixel[0] + 'px',
      top: (pixel[1] - 15) + 'px'
    });
    var feature = mapa.map.forEachFeatureAtPixel(pixel, function(feature, layer) {
      return feature;
    });
    if (feature) {
      mapa.info.tooltip('hide')
          .attr('data-original-title', feature.attributes.name)
          .tooltip('fixTitle')
          .tooltip('show');
    } else {
      mapa.info.tooltip('hide');
    }
  },
  crearruta:function(){
      window.location.href="/mapa/ruta";
  },
  cancelar:function(objeto,tipo){
    var id=$(objeto).data("id");
    var suc=function(){
      var funsuccess=function(data){
        $("#myTab li.active a").trigger('click');;
      }
      var funcomplete=function(){
      }
      var funbefore=function(){
        
      }
      var funerror=function(){

      }
      general.cargarajax('/mapa/cancelar', {id: id,tipo:tipo},funsuccess,funbefore,funcomplete,funerror,"html");
    }
    var mensaje=(tipo==1)?"la ruta":"el documento";
    general.confirmar(
      '¿Desea cancelar '+mensaje+'?',
      "Cambiar Estado",
      {'ls':"Aceptar",'success':suc,'lc':"Cancelar"})    
  },
  obtenerDatos:function(opcion){
    if(this.enable_tab){
      switch(opcion){
        case 1:$("#htitulo").html("Listado de rutas");break;
        case 2:$("#htitulo").html("Listado de documentos en ruta");break;
      }
      this.enable_tab=false;
      tab = $('#tab');
      setTimeout(function(){
        $("#myTab li a").removeAttr('data-toggle');
      },100);
      var funsuccess=function(data){
        tab.html(data);
        tabla.generar();
      }
      var funcomplete=function(){
        mapa.enable_tab=true;
        $("#myTab li a").attr('data-toggle',"tab");
        $("#tab").css('overflow','');
        tab.slideDown(300,function(){
          
        });
        setTimeout(function(){
          tabla.refresh();
          
        },200);
        
      }
      var funbefore=function(){
        tab.slideUp(300);
      }
      var funerror=function(){

      }
      general.cargarajax('/mapa/listado', {'opcion': opcion},funsuccess,funbefore,funcomplete,funerror,"html");
    }
  }
};
      
      
