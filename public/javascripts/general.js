var general = {
	enable_tab:true,
	sProcessing:"Procesando...",
    sLengthMenu:"Mostrar _MENU_ registros",
    sZeroRecords:"No se encontraron resultados",
    sEmptyTable:"Ningún dato disponible en esta tabla",
    sInfo:"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    sInfoEmpty:"Mostrando registros del 0 al 0 de un total de 0 registros",
    sInfoFiltered:"(filtrado de un total de _MAX_ registros)",
    sInfoPostFix:"",
    sSearch:"Buscar:",
    sUrl:"",
    sInfoThousands:",",
    sLoadingRecords:"Cargando...",
    sFirst:"Primero",
    sLast:"Último",
    sNext:"Siguiente",
    sPrevious:"Anterior",
    sSortAscending:": Activar para ordenar la columna de manera ascendente",
   	sSortDescending:": Activar para ordenar la columna de manera descendente",
	Encriptar:function(texto){
		var semilla = $("#token").val();
		var suma = 0;
		var newtexto = "";
		for(x=0; x < semilla.length; x++)
			suma += semilla.charCodeAt(x);
		semilla = suma.toString();
		suma = 0;
		for(z=0; z < semilla.length; z++)
			suma += parseInt(semilla.charAt(z));
		semilla = parseInt(suma);
		for(y=0; y < texto.length; y++)
		{
			if(texto.charCodeAt(y) + semilla > 126)
				suma = ((texto.charCodeAt(y) + semilla) - 126) + 31;
			else
				suma = (texto.charCodeAt(y) + semilla);
			newtexto += String.fromCharCode(suma);
		}
		return newtexto;
	},
	/*select * from `fyssonco_fysson`.`v_coordenadas_hoy` as v1 inner join 
	(select max(id_coordenada) as id_coordenada from `fyssonco_fysson`.`v_coordenadas_hoy` 

	group by (id_vehiculo)) as v2 on v1.id_coordenada=v2.id_coordenada*/

	guardar:function(datos,stay,fn_name,id){
		if($("#guardar").attr('disabled')!="disabled"){
			var controller=($("#new").data('controller'));
			if(typeof stay =='undefined')
				stay = 0;
			$("#guardar").attr('disabled','disabled');
			var funsuccess=function(data){
				if(data["tipo"]==1){
					eval("var mensaje="+controller.toLowerCase()+".mensajecorrecto");
					general.alerta(mensaje,1);
					if(typeof id != 'undefined')
						id.val(data["id"]);
					if(stay==1){
						fn_name();
					}else{
		                setTimeout(function(){ window.location.reload(true); },2000);	
					}
				}
				if(data["tipo"]==2){
					eval("var mensaje="+controller.toLowerCase()+".mensajeincorrecto");
					mensaje+="<br>Detalles: "+data["mensaje"];
					$("#guardar").removeAttr('disabled');
					general.alerta(mensaje,4);
				}
				if(data["tipo"]==4){
					setTimeout(function(){ window.location.reload(true); },1000);
				}
			}//aguanta se apagaron los audifonos ve probando
			var funcomplete=function(){
				
			}
			var funbefore=function(){

			}
			var funerror=function(){

			}
			this.cargarajax('/'+controller+'/guardar',{'datos':datos},funsuccess,funbefore,funcomplete,funerror,"json");
		}
	},
	cargartabla:function(){
		$('.dTable').DataTable({
				"language":{
				    "sProcessing":     general.sProcessing,
				    "sLengthMenu":     general.sLengthMenu,
				    "sZeroRecords":    general.sZeroRecords,
				    "sEmptyTable":     general.sEmptyTable,
				    "sInfo":           general.sInfo,
				    "sInfoEmpty":      general.sInfoEmpty,
				    "sInfoFiltered":   general.sInfoFiltered,
				    "sInfoPostFix":    general.sInfoPostFix,
				    "sSearch":         general.sSearch,
				    "sUrl":            general.sUrl,
				    "sInfoThousands":  general.sInfoThousands,
				    "sLoadingRecords": general.sLoadingRecords,
				    "oPaginate": {
				        "sFirst":    general.sFirst,
				        "sLast":     general.sLast,
				        "sNext":     general.sNext,
				        "sPrevious": general.sPrevious
				    },
				    "oAria": {
				        "sSortAscending":  general.sSortAscending,
				        "sSortDescending": general.sSortDescending
				    }
				},
				responsive: true
			});
	},
	alerta:function(mensaje,tipo){
		switch(tipo){
			case 1:var id="success";break;
			case 2:var id="info";break;
			case 3:var id="warning";break;
			case 4:var id="danger";break;
		}
		$("#"+id).html(mensaje);
		$("#"+id).slideDown(300);
		setTimeout(function(){
			$("#"+id).slideUp(300);
		},4000);
	},

	confirmar:function(message,title,fun){
		bootbox.dialog({
			message: message,
			title: title,
			buttons: {
				success: {
					label: fun['ls'],
					className: "btn-success",
					callback: fun['success']
				},
				danger: {
					label: fun['lc'],
					className: "btn-danger",
					callback: fun['close']
				}
			}
		});
	},
	loading:function(){
		 $('#loading').fadeToggle();
	},
	obtenerFormulario:function(id){
		var controller=$("#new").data('controller');
		var funsuccess=function(data){
			$('#Formulario #contenido').html(data);
			$('#Formulario').modal();
		}
		var funcomplete=function(){
			if(id!=0)
				$("#titulo").html("Editar "+$("#new").data('controller'));
			else
				$("#titulo").html("Añadir "+$("#new").data('controller'));
			console.log(controller.toLowerCase()+".iniciar()");
			eval(controller.toLowerCase()+".iniciar()");
		}
		var funbefore=function(){

		}
		var funerror=function(){

		}
		this.cargarajax('/'+controller+'/obtenerformulario',{'id': id},funsuccess,funbefore,funcomplete,funerror,"html");
	},
	obtenerDatos:function(activo){

		if(this.enable_tab){
			console.log("entre");
			this.enable_tab=false;
			tab = $('#tab');
			setTimeout(function(){
				$("#myTab li a").removeAttr('data-toggle');
			},100);
			var controller=$("#new").data('controller');
			var funsuccess=function(data){
				tab.html(data);
				tabla.generar();
			}
			var funcomplete=function(){
				general.enable_tab=true;
				$("#myTab li a").attr('data-toggle',"tab");
				$("#tab").css('overflow','');
				tab.slideDown(300,function(){
					
				});
				setTimeout(function(){
					tabla.refresh();
					
				},200);
				
			}
			var funbefore=function(){
				tab.slideUp(300);
			}
			var funerror=function(){

			}
			this.cargarajax('/'+controller+'/', {'estado': activo},funsuccess,funbefore,funcomplete,funerror,"html");
		}
		
	},
	cargarajax:function(direccion,datos,funsuccess,funbefore,funcomplete,funerror,type){
		$.ajax({
			type: 'POST',
			url: direccion,
			data:datos,
            dataType: type,
			success: function(data){
				funsuccess(data);
			},
			beforeSend:funbefore(),
			complete:funcomplete()
		});
	},
	cambiarestado:function(id,object){
		var controller=$("#new").data('controller');
		var estado=$(object).data("estado");
		var suc=function(){
			var funsuccess=function(data){
				$("#myTab li.active a").trigger('click');;
			}
			var funcomplete=function(){
			}
			var funbefore=function(){
				
			}
			var funerror=function(){

			}
			general.cargarajax('/'+controller+'/cambiarestado', {id: id},funsuccess,funbefore,funcomplete,funerror,"html");
		}
		var mensaje=eval(((estado==1)?controller.toLowerCase()+'.mensajedesactivar':controller.toLowerCase()+'.mensajeactivar'));
		general.confirmar(
			mensaje+'?',
			"Cambiar Estado",
			{'ls':"Cambiar Estado",'success':suc,'lc':"Cancelar"})		
	},
	post:function(path, params, method,parent) {
	    method = method || "post"; // Set method to post by default if not specified.
	    parent = parent || "unico";
	    // The rest of this code assumes you are not using a library.
	    // It can be made less wordy if you use one.
	    var form = document.createElement("form");
	    form.setAttribute("method", method);
	    form.setAttribute("action", path);

	    for(var key in params) {
	        if(params.hasOwnProperty(key)) {
	            var hiddenField = document.createElement("input");
	            hiddenField.setAttribute("type", "hidden");
	            hiddenField.setAttribute("name", key);
	            hiddenField.setAttribute("value", params[key]);

	            form.appendChild(hiddenField);
	         }
	    }
	    if(parent!="unico")
	    	parent.document.body.appendChild(form);
	    else
	    	document.body.appendChild(form);
	    
	    console.log(parent.document);
	    form.submit();
	}
}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (typeof o[this.name]!="undefined") {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};