var perfil={
	iniciar:function(){
		$("#guardar").on('click',function(){
			$('#miformulario').validator().on('submit', function (e) {
			  	if (e.isDefaultPrevented()) {
			    	
			  	} else {
					var form=$("#miformulario").serializeObject();
					if(typeof form.contrasena!="undefined")
						form.contrasena=general.Encriptar(form.contrasena);
					form.confirmarcontrasena="";
					form.token="";
					perfil.guardar(form);
			  	}
			  	return false;
			})
			$("#miformulario").submit();
		});
		setTimeout(function(){
			$("#miformulario").validator();
			$("#cambiar").on("click",function(){
				perfil.desbloquearcontrasena();
			});
			$("#usuario").on("change",function(){
				perfil.comprobar($("#usuario").val());
			});
			$("#correo").on("change",function(){
				perfil.comprobarEmail($("#correo").val());
			});
		},800);
	},
	obtenerformulario:function(){
		var funsuccess=function(data){
			$('#Formulario #contenido').html(data);
			$('#Formulario').modal();
		}
		var funcomplete=function(){
			perfil.iniciar();
		}
		var funbefore=function(){

		}
		var funerror=function(){

		}
		general.cargarajax('/index/obtenerformulario',{},funsuccess,funbefore,funcomplete,funerror,"html");
	},
	desbloquearcontrasena:function(){
		$("#contrasena").attr("required","required");
		$("#contrasena").attr("disabled",false);
		$("#confirmarcontrasena").attr("required","required");
		$("#confirmarcontrasena").attr("disabled",false);
	},
	guardar:function(datos){
		if($("#guardar").attr('disabled')!="disabled"){
			$("#guardar").attr('disabled','disabled');
			var funsuccess=function(data){
				if(data["tipo"]==1){
					var mensaje="<strong>Correcto</strong> El perfil se guardó correctamente.";
					general.alerta(mensaje,1);
		            setTimeout(function(){ window.location.reload(true); },2000);	
					
				}
				if(data["tipo"]==2){
					var mensaje="<strong>InCorrecto</strong> El perfil no se pudo guardar correctamente.";
					mensaje+="<br>Detalles: "+data["mensaje"];
					$("#guardar").removeAttr('disabled');
					general.alerta(mensaje,4);
				}
				if(data["tipo"]==4){
					setTimeout(function(){ window.location.reload(true); },1000);
				}
			}
			var funcomplete=function(){
				
			}
			var funbefore=function(){

			}
			var funerror=function(){

			}
			general.cargarajax('/index/guardarperfil',{'datos':datos},funsuccess,funbefore,funcomplete,funerror,"json");
		}
	},
	comprobar:function(usuario){
		var id=$("#id_usuario").val();
		var funsuccess=function(data){
			if(data=="1"){
				$("#error_usuario").html("Este usuario ya existe");
				$("#error_usuario").parent().addClass("has-error has-danger");
			}
			if(data=="2"){
				$("#error_usuario").html("");
				$("#error_usuario").parent().removeClass("has-error has-danger");
			}
		}
		var funcomplete=function(){

		}
		var funbefore=function(){

		}
		var funerror=function(){

		}
		general.cargarajax('/Usuario/existe',{'usuario':usuario,'id':id},funsuccess,funbefore,funcomplete,funerror,"html");
	},
	comprobarEmail:function(email){
		var id=$("#id_usuario").val();
		var funsuccess=function(data){
			if(data=="1"){
				$("#error_email").html("Este email ya se encuentra registrado");
				$("#error_email").parent().addClass("has-error has-danger");
			}
			if(data=="2"){
				$("#error_email").html("");
				$("#error_email").parent().removeClass("has-error has-danger");
			}
		}
		var funcomplete=function(){

		}
		var funbefore=function(){

		}
		var funerror=function(){

		}
		general.cargarajax('/Usuario/existeemail',{'email':email,'id':id},funsuccess,funbefore,funcomplete,funerror,"html");
	}
}