var cuenta={
	mensajecorrecto:"<strong>Correcto</strong> Esta cuenta se guardó correctamente.",
	mensajeincorrecto:"<strong>InCorrecto</strong> Esta cuenta no se pudo guardar correctamente.",
	mensajeactivar:"¿Desea activar este cuenta",
	mensajedesactivar:"¿Desea desactivar este cuenta",
	iniciar:function(){
		//$("#miformulario").validator();
		$("#guardar").on('click',function(){
			$('#miformulario').validator().on('submit', function (e) {
			  	if (e.isDefaultPrevented()) {
			    	$("#guardar").removeAttr('disabled');
			  	} else {
					var form=$("#miformulario").serializeObject();
					general.guardar(form);
			  	}
			  	return false;
			})
			$("#miformulario").submit();
		});
		setTimeout(function(){
			$('.chosen-select').chosen();
		    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
			$("#miformulario").validator();
			
		},500);
	}
}