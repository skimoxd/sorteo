var boleto={
	mensajecorrecto:"<strong>Correcto</strong> Este boleto se guardó correctamente.",
	mensajeincorrecto:"<strong>InCorrecto</strong> Este boleto no se pudo guardar correctamente.",
	mensajeactivar:"¿Desea activar este boleto",
	mensajedesactivar:"¿Desea desactivar este boleto",
	existe:1,
	noguardar:0,
	iniciar:function(){
		//$("#miformulario").validator();
		$("#guardar").on('click',function(){
			$('#miformulario').validator().on('submit', function (e) {
			  	if (e.isDefaultPrevented()) {
			    	
			  	} else {
					var form=$("#miformulario").serializeObject();
					if($("#id_boleto").val()!=0 && $("#archivo").val()!="")
						boleto.subirarchivo();
					else
						general.guardar(form);

			  	}
			  	return false;
			})
			$("#miformulario").submit();
		});
		setTimeout(function(){
			$('.chosen-select').chosen();
		    $('.chosen-select-deselect').chosen();
			$("#miformulario").validator();
			$("#num_boleto").on("change",function(){
				boleto.comprobarboleto($("#num_boleto").val());
			});
			$("#id_sorteo").change(function(){
				boleto.obtenerboletos($(this).val());
			});
			$("#id_lada").on('change',function(){
				$("#btnlada").text('+'+$( "#id_lada option:selected" ).data('num'));
			});
			$("#archivo").on('change',function(){
				var input = $(this);
  				var numFiles = input.get(0).files ? input.get(0).files.length : 1;
  				var label = input.val().replace(/\\/g,'/').replace(/.*\//,'');
  				input.trigger('fileselect',[numFiles,label]);
			});
			$("#archivo").on("fileselect",function(event,numFiles,label){
				var input = $(this).parents('.input-group').find(':text');
    			var log = numFiles > 1 ? numFiles + ' files selected' : label;
    			if(input.length){ input.val(log); }else{ if (log) alert(log); }
			});
		},500);
	},
	subirarchivo:function(){
		var inputFileImage = document.getElementById("archivo");
		var file = inputFileImage.files[0];
		var data = new FormData();
		data.append('boleto',$("#id_boleto").val());
		data.append('archivo',file);
		$.ajax({
			url: "/boleto/subirarchivo",        // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: data, 			  // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				if(data!="1"){
					boleto.noguardar=1;
					general.alerta(data,4);
				}
				else{
					var form=$("#miformulario").serializeObject();
					general.guardar(form);
				}
				/*$(".upload-msg").html(data);
				window.setTimeout(function() {
				$(".alert-dismissible").fadeTo(500, 0).slideUp(500, function(){
				$(this).remove();
				});	}, 5000);*/
			}
		});
	},
	comprobarboleto:function(boleto){
		var id=$("#id_boleto").val();
		var sorteo=$("#id_sorteo").val();
		var funsuccess=function(data){
			if(data=="1"){
				$("#error_boleto").html("Este boleto ya existe");
				$("#error_boleto").parent().addClass("has-error has-danger");
			}
			if(data=="2"){
				$("#error_boleto").html("");
				$("#error_boleto").parent().removeClass("has-error has-danger");
			}
		}
		var funcomplete=function(){

		}
		var funbefore=function(){

		}
		var funerror=function(){

		}
		general.cargarajax('/boleto/existe',{'boleto':boleto,'id':id,'sorteo':sorteo},funsuccess,funbefore,funcomplete,funerror,"html");
	},
	obtenerboletos:function(sorteo)
	{
		var id=sorteo;
		var funsuccess=function(data){
			$("#div_boletos").slideDown(300);
			/*var opcion="";
			data.forEach(function(value){
				opcion+="<option value="+value+">"+value+"</option>";
				
			});*/
			$("#num_boleto").html(data['opcion']);
			$("#num_boleto").trigger("chosen:updated");
		}
		var funcomplete=function(){

		}
		var funbefore=function(){
			$("#div_boletos").slideUp(300);	
		}
		var funerror=function(){

		}
		general.cargarajax('/boleto/obtenerboletos',{'id':id,'sorteo':sorteo},funsuccess,funbefore,funcomplete,funerror,"json");
	}
}