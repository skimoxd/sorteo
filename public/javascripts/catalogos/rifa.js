var rifa={
	mensajecorrecto:"<strong>Correcto</strong> Esta rifa se guardó correctamente.",
	mensajeincorrecto:"<strong>InCorrecto</strong> Esta rifa no se pudo guardar correctamente.",
	mensajeactivar:"¿Desea activar esta rifa",
	mensajedesactivar:"¿Desea desactivar esta rifa",
	bloqueo:0,
	iniciar:function(){
		$("#guardar").on('click',function(){
			$('#miformulario').validator().on('submit', function (e) {
			  	if (e.isDefaultPrevented()) {
			    	
			  	} else {
					var form=$("#miformulario").serializeObject();
					if(typeof form.contrasena!="undefined")
						form.contrasena=general.Encriptar(form.contrasena);
					form.confirmarcontrasena="";
					form.token="";
					general.guardar(form,0,null,$("#id_sorteo"));
					rifa.subirarchivo();
			  	}
			  	return false;
			})
			$("#miformulario").submit();
		});
		setTimeout(function(){
			$('.chosen-select').chosen();
			$('.datepicker').datepicker({
		        	language:"es"
		        });
		    $('.chosen-select-deselect').chosen({ allow_single_deselect: true, no_results_text: "No se encontraron tipos de empleado. Presione enter para añadirlo "});
			$("#usuario").on("change",function(){
				usuario.comprobar($("#usuario").val());
			});
			$("#correo").on("change",function(){
				usuario.comprobarEmail($("#correo").val());
			});
			$("#cambiar").on("click",function(){
				usuario.desbloquearcontrasena();
			});
			$("#archivo").on('change',function(){
				var input = $(this);
  				var numFiles = input.get(0).files ? input.get(0).files.length : 1;
  				var label = input.val().replace(/\\/g,'/').replace(/.*\//,'');
  				input.trigger('fileselect',[numFiles,label]);
			});
			$("#archivo").on("fileselect",function(event,numFiles,label){
				var input = $(this).parents('.input-group').find(':text');
    			var log = numFiles > 1 ? numFiles + ' files selected' : label;
    			if(input.length){ input.val(log); }else{ if (log) alert(log); }
			});
			$("#miformulario").validator();
		},800);
	},
	subirarchivo:function(){
		if(rifa.bloqueo==0){
			rifa.bloqueo=1;
			var inputFileImage = document.getElementById("archivo");
			var file = inputFileImage.files[0];
			var data = new FormData();
			data.append('sorteo',$("#id_sorteo").val());
			data.append('archivo',file);
			$.ajax({
				url: "/rifa/subirarchivo",        // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: data, 			  // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
				}
			});
		}

	},
	comprobar:function(usuario){
		var id=$("#id_usuario").val();
		var funsuccess=function(data){
			if(data=="1"){
				$("#error_usuario").html("Este usuario ya existe");
				$("#error_usuario").parent().addClass("has-error has-danger");
			}
			if(data=="2"){
				$("#error_usuario").html("");
				$("#error_usuario").parent().removeClass("has-error has-danger");
			}
		}
		var funcomplete=function(){

		}
		var funbefore=function(){

		}
		var funerror=function(){

		}
		general.cargarajax('/Usuario/existe',{'usuario':usuario,'id':id},funsuccess,funbefore,funcomplete,funerror,"html");
	},

}