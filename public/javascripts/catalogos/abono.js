var abono={
	mensajecorrecto:"<strong>Correcto</strong> Este abono se guardó correctamente.",
	mensajeincorrecto:"<strong>InCorrecto</strong> Este abono no se pudo guardar correctamente.",
	mensajeactivar:"¿Desea activar este abono",
	mensajedesactivar:"¿Desea desactivar este abono",
	noguardar:0,
	cantidad:1,
	rifas:[],
	iniciar:function(){
		//$("#miformulario").validator();
		$("#guardar").on('click',function(){
			$('#miformulario').validator().on('submit', function (e) {
			  	if (e.isDefaultPrevented()) {
			    	$('.chosen-select').trigger("chosen:updated");
			    	console.log("fallo");
			  	} else {
					var form=$("#miformulario").serializeObject();
					if($("#id_abono").val()!=0 && $("#archivo").val()!="")
						abono.subirarchivo();
					else
						general.guardar(form);
			  	}
			  	return false;
			})
			$("#miformulario").submit();
		});
		setTimeout(function(){
			$('.chosen-select').chosen();
		    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
		    $('.datepicker').datepicker({
		        	language:"es"
		        });
		    $("#bagregarfab").on("click",function(){
		    	abono.cantidad++;
		    	var opciones="";
				abono.rifas.forEach(function(a){
					opciones+="<option  value="+a["id_sorteo"]+" >"+a["descripcion"]+"</option>";
				});
				
		    	$("#div_abonos").append("<div style=\"clear: both;\">\
		    		<div class=\"form-group col-md-4\">\
                    <select class=\"chosen-select\" id=\"abono_sorteo_"+abono.cantidad+"\" name=\"abono_sorteo\" tabindex=\"2\" onchange=\"abono.obtenerboletosporrifa(this,'"+abono.cantidad+"')\">\
                        <option value=\"\" disabled selected>Seleccione el sorteo</option>\
                        "+opciones+"\
                    </select>\
                </div>\
                <div class=\"form-group col-md-4\" id=\"div_boletos_"+abono.cantidad+"\">\
                    <select class=\"chosen-select\"  id=\"abono_boleto_"+abono.cantidad+"\" name=\"abono_boleto\" required onchange=\"abono.agregarboleto(this,'"+abono.cantidad+"')\">\
                        <option value=\"\">Seleccione un boleto</option>\
                    </select>\
                    <div class=\"help-block with-errors\" id=\"error_boleto_"+abono.cantidad+"\"></div>\
                </div>\
                <div class=\"form-group col-md-4\">\
                    <div class=\"input-group\">\
                        <span class=\"input-group-btn\"><button type=\"button\" class=\"btn btn-secundary \" style=\"color: #000; \" disabled onclick=\"return false;\" >$</button>\
                        </span>\
                        <input type=\"text\" class=\"form-control\" id=\"abono_cantidad_"+abono.cantidad+"\" name=\"abono_cantidad\" placeholder=\"Precio\" value=\"\" required  onchange=\"abono.validarpgo('"+abono.cantidad+"')\">\
                        <span class=\"input-group-btn  disabled\">\
                            <button type=\"button\"  class=\"btn btn-secundary closeabono\" style=\"background-color: #C70039; \" id=\"close  \">X</button>\
                        </span>\
                    </div>\
                    <div class=\"help-block with-errors\" id=\"error_importe_"+abono.cantidad+"\"></div>\
                </div>");
                $("#miformulario").validator("update");
                $('.chosen-select').chosen();
		    	$('.chosen-select-deselect').chosen({ allow_single_deselect: true });
		    	$(".closeabono").off("click").on("click",function(){

					$(this).parent().parent().parent().parent().remove();
				});
		    });
			$("#miformulario").validator();
			$("#id_sorteo").change(function(){
				abono.obtenerboletos($(this).val());
			});
			
			$("#archivo").on('change',function(){
				var input = $(this);
  				var numFiles = input.get(0).files ? input.get(0).files.length : 1;
  				var label = input.val().replace(/\\/g,'/').replace(/.*\//,'');
  				input.trigger('fileselect',[numFiles,label]);
			});
			$("#archivo").on("fileselect",function(event,numFiles,label){
				var input = $(this).parents('.input-group').find(':text');
    			var log = numFiles > 1 ? numFiles + ' files selected' : label;
    			if(input.length){ input.val(log); }else{ if (log) alert(log); }
			});
			$("#importe").change(function(){
				if($(this).val()>0){
					abono.validarpago($(this).val());
				}
				else{
					setTimeout(function(){
						$("#error_importe").parent().addClass("has-error has-danger");
						$("#error_importe").html("El importe no puede tener valor 0");
					
					},50);
					
				}
			});
		},500);
	},
	agregarboleto:function(objeto,num){
		var i=1;
		var b=0;
		boleto=$(objeto).val();
		$("#error_boleto_"+num).html("");
		$("#error_boleto_"+num).parent().removeClass("has-error has-danger");
		for(i=1;i<=abono.cantidad;i++){
			if(num!=i){
				b=$("#abono_boleto_"+i).val();
				
				if(b==boleto){
					$("#error_boleto_"+num).html("El boleto no se puede repetir");
					$("#error_boleto_"+num).parent().addClass("has-error has-danger");
				}
			}

		}
	},
	subirarchivo:function(){
		var inputFileImage = document.getElementById("archivo");
		var file = inputFileImage.files[0];
		var data = new FormData();
		data.append('abono',$("#id_abono").val());
		data.append('archivo',file);
		$.ajax({
			url: "/abono/subirarchivo",        // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: data, 			  // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				if(data!="1"){
					abono.noguardar=1;
					general.alerta(data,4);
				}
				else{
					var form=$("#miformulario").serializeObject();
					general.guardar(form);
				}
				/*$(".upload-msg").html(data);
				window.setTimeout(function() {
				$(".alert-dismissible").fadeTo(500, 0).slideUp(500, function(){
				$(this).remove();
				});	}, 5000);*/
			}
		});
	},
	obtenerboletosporrifa:function(sorteo,num){
		var id=$(sorteo).val();
		var funsuccess=function(data){
			$("#div_boletos_"+num).slideDown(300);
			var opcion="<option value=\"\">Seleccione un boleto</option>";
			$("#abono_boleto_"+num).html(opcion);
			data.forEach(function(value){
				opcion+="<option value="+value["id_boleto"]+">"+value["num_boleto"]+" - "+value["nombre_completo"]+"</option>";
				$("#abono_boleto_"+num).html(opcion);
				$("#abono_boleto_"+num).trigger("chosen:updated");
			});
		}
		var funcomplete=function(){

		}
		var funbefore=function(){
			$("#div_boletos_"+num).slideUp(300);	
		}
		var funerror=function(){

		}
		general.cargarajax('/abono/obtenerboletos',{'id':id},funsuccess,funbefore,funcomplete,funerror,"json");
	},
	obtenerboletos:function(sorteo)
	{
		var id=sorteo;
		var funsuccess=function(data){
			$("#div_boletos").slideDown(300);
			var opcion="<option value=\"\">Seleccione un boleto</option>";
			$("id_boleto").html(opcion);
			data.forEach(function(value){
				opcion+="<option value="+value["id_boleto"]+">"+value["num_boleto"]+" - "+value["nombre_completo"]+"</option>";
				$("#id_boleto").html(opcion);
				$("#id_boleto").trigger("chosen:updated");
			});
		}
		var funcomplete=function(){

		}
		var funbefore=function(){
			$("#div_boletos").slideUp(300);	
		}
		var funerror=function(){

		}
		general.cargarajax('/abono/obtenerboletos',{'id':id},funsuccess,funbefore,funcomplete,funerror,"json");
	},
	validarpgo:function(num){
		var id_abono=$("#id_abono").val();
		var id=$("#abono_boleto_"+num).val();
		if(id!="")
		{
			importe=$("#abono_cantidad_"+num).val();
			var funsuccess=function(data){
				console.log(data);
				if(data=="1"){
					$("#error_importe_"+num).html("El importe excedio el monto total del boleto");
					$("#error_importe_"+num).parent().addClass("has-error has-danger");
				}
				if(data=="0"){
					$("#error_importe_"+num).html("");
					$("#error_importe_"+num).parent().removeClass("has-error has-danger");
					abono.calculartotal();
				}
			}
			var funcomplete=function(){

			}
			var funbefore=function(){

			}
			var funerror=function(){

			}
			general.cargarajax('/abono/validar_pago',{'importe':importe,'id':id,'id_abono':id_abono},funsuccess,funbefore,funcomplete,funerror,"html");
		}
	},
	calculartotal:function(){
		var total=0;
		for(i=1;i<=abono.cantidad;i++){
			if($("#abono_cantidad_"+i).val()!="")
				total+=parseFloat($("#abono_cantidad_"+i).val());

		}
		$("#total").val(total.toFixed(2));
	},
	validarpago:function(importe){
		var id=$("#id_boleto").val();
		var id_abono=$("#id_abono").val();
		var funsuccess=function(data){
			if(data=="1"){
				$("#error_importe").html("El importe excedio el monto total del boleto");
				$("#error_importe").parent().addClass("has-error has-danger");
			}
			if(data=="0"){
				$("#error_importe").html("");
				$("#error_importe").parent().removeClass("has-error has-danger");
			}
		}
		var funcomplete=function(){

		}
		var funbefore=function(){

		}
		var funerror=function(){

		}
		general.cargarajax('/abono/validar_pago',{'importe':importe,'id':id,'id_abono':id_abono},funsuccess,funbefore,funcomplete,funerror,"html");
	},
	cambiarestado:function(id,object){
		var estado=$(object).data("estado");

		var suc=function(){
			var funsuccess=function(data){
				console.log(data);
				//
				if(data["tipo"]==1){
					$("#myTab li.active a").trigger('click');
				}
				if(data["tipo"]==2){
					var mensaje=(estado==1)?"Este abono no se pudo desactivar":"Este abono no se pudo activar";
					mensaje+="<br>Detalles: "+data["mensaje"];
					general.alerta(mensaje,4);
				}
				if(data["tipo"]==4){
					setTimeout(function(){ window.location.reload(true); },1000);
				}
			}
			var funcomplete=function(){
			}
			var funbefore=function(){
				
			}
			var funerror=function(){

			}
			general.cargarajax('/abono/cambiarestado', {id: id},funsuccess,funbefore,funcomplete,funerror,"json");
		}
		var mensaje=(estado==1)?abono.mensajedesactivar:abono.mensajeactivar;
		general.confirmar(
			mensaje+'?',
			"Cambiar Estado",
			{'ls':"Cambiar Estado",'success':suc,'lc':"Cancelar"})		
	}
}