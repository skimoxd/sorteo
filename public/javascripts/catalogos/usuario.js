var usuario={
	mensajecorrecto:"<strong>Correcto</strong> Este usuario se guardó correctamente.",
	mensajeincorrecto:"<strong>InCorrecto</strong> Este usuario no se pudo guardar correctamente.",
	mensajeactivar:"¿Desea activar este usuario",
	mensajedesactivar:"¿Desea desactivar este usuario",
	iniciar:function(){
		$("#guardar").on('click',function(){
			$('#miformulario').validator().on('submit', function (e) {
			  	if (e.isDefaultPrevented()) {
			    	
			  	} else {
					var form=$("#miformulario").serializeObject();
					if(typeof form.contrasena!="undefined")
						form.contrasena=general.Encriptar(form.contrasena);
					form.confirmarcontrasena="";
					form.token="";
					general.guardar(form);
			  	}
			  	return false;
			})
			$("#miformulario").submit();
		});
		setTimeout(function(){
			$("#usuario").on("change",function(){
				usuario.comprobar($("#usuario").val());
			});
			$("#correo").on("change",function(){
				usuario.comprobarEmail($("#correo").val());
			});
			$("#cambiar").on("click",function(){
				usuario.desbloquearcontrasena();
			});
			$("#miformulario").validator();
		},800);
	},
	desbloquearcontrasena:function(){
		$("#contrasena").attr("required","required");
		$("#contrasena").attr("disabled",false);
		$("#confirmarcontrasena").attr("required","required");
		$("#confirmarcontrasena").attr("disabled",false);
	},
	comprobar:function(usuario){
		var id=$("#id_usuario").val();
		var funsuccess=function(data){
			if(data=="1"){
				$("#error_usuario").html("Este usuario ya existe");
				$("#error_usuario").parent().addClass("has-error has-danger");
			}
			if(data=="2"){
				$("#error_usuario").html("");
				$("#error_usuario").parent().removeClass("has-error has-danger");
			}
		}
		var funcomplete=function(){

		}
		var funbefore=function(){

		}
		var funerror=function(){

		}
		general.cargarajax('/Usuario/existe',{'usuario':usuario,'id':id},funsuccess,funbefore,funcomplete,funerror,"html");
	},
	comprobarEmail:function(email){
		var id=$("#id_usuario").val();
		var funsuccess=function(data){
			if(data=="1"){
				$("#error_email").html("Este email ya se encuentra registrado");
				$("#error_email").parent().addClass("has-error has-danger");
			}
			if(data=="2"){
				$("#error_email").html("");
				$("#error_email").parent().removeClass("has-error has-danger");
			}
		}
		var funcomplete=function(){

		}
		var funbefore=function(){

		}
		var funerror=function(){

		}
		general.cargarajax('/Usuario/existeemail',{'email':email,'id':id},funsuccess,funbefore,funcomplete,funerror,"html");
	}
}