var almacen={
	cargarmovimiento:function(){
		$("#titulo").html("Añadir movimiento");
		var funsuccess=function(data){
			$('#Formulario #contenido').html(data);
			$('#Formulario').modal();
		}
		var funcomplete=function(){
			almacen.movimiento();
		}
		var funbefore=function(){

		}
		var funerror=function(){

		}
		general.cargarajax('/inventario/addmovimiento',{},funsuccess,funbefore,funcomplete,funerror,"html");
	},
	movimiento:function(){
		$("#guardar").on('click',function(){
			$('#miformulario').validator().on('submit', function (e) {
			  	if (e.isDefaultPrevented()) {
			    	
			  	} else {
					var datos=$("#miformulario").serializeObject();
					$("#guardar").attr('disabled','disabled');
					var funsuccess=function(data){
						if(data["tipo"]==1){
							var mensaje="El movimiento se guardo correctamente";
							general.alerta(mensaje,1);
				            setTimeout(function(){ window.location.reload(true); },2000);	
						}
						if(data["tipo"]==2){
							var mensaje="El movimiento no se pudo guardar correctamente";
							mensaje+="<br>Detalles: "+data["mensaje"];
							$("#guardar").removeAttr('disabled');
							general.alerta(mensaje,4);
						}
						if(data["tipo"]==4){
							setTimeout(function(){ window.location.reload(true); },1000);
						}
					}
					var funcomplete=function(){
						
					}
					var funbefore=function(){

					}
					var funerror=function(){

					}
					general.cargarajax('/inventario/guardarmovimiento',{'datos':datos},funsuccess,funbefore,funcomplete,funerror,"json");
			  	}
			  	return false;
			})
			$("#miformulario").submit();
		});
		setTimeout(function(){
			$('.chosen-select').chosen();
	    	$('.chosen-select-deselect').chosen({ allow_single_deselect: true });
			$('.datepicker').datepicker({
	        	language:"es"
	        });
	    },500);
	}

}