var mapa={
  map:null,
  direcciones:[],
  vectorSource:null,
  paquetes:[],
  paquetesbyid:[],
  apuntador:0,
  adjustment:null,
  error:0,
  marker:null,
  directionsDisplay:"",
  directionsService:"",
  style: {
      route: new ol.style.Style({
        stroke: new ol.style.Stroke({
          width: 6, color: [40, 40, 40, 0.8]
        })
      }),
      icon: new ol.style.Style({
        image: new ol.style.Icon({
          anchor: [0.5, 0.5],
          src: "/imagenes/paquete.png"
        })
      })
    },
  guardar:function(){
    if(mapa.paquetes.length>0){
      var ruta=$("#miformulario").serializeObject();
      ruta.paquetes=mapa.paquetes;
      var funsuccess=function(data){
        if(data["tipo"]==1){
          general.alerta("La ruta se guardo correctamente",1);
          setTimeout(function(){ 
            window.location.reload(true); 
          },2000);
        }
        if(data["tipo"]==2){
          mensaje="La ruta no se pudo guardar correctamente";
          mensaje+="<br>Detalles: "+data["mensaje"];
          general.alerta(mensaje,4);
        }
        if(data["tipo"]==4){
          setTimeout(function(){ window.location.reload(true); },1000);
        }
      }
      var funcomplete=function(){

      }
      var funbefore=function(){

      }
      var funerror=function(){

      }
      general.cargarajax('/mapa/guardarruta',{'datos':ruta},funsuccess,funbefore,funcomplete,funerror,"json");
    }
    else
      general.alerta("Agregue al menos un paquete",4);
  },
  limpiar:function(){
    $("#id_vehiculo option[value='']").attr("selected","selected");
    $("#id_vehiculo option[value='']").removeAttr("selected");
    $("#id_repartidor option[value='']").attr("selected","selected");
    $("#id_repartidor option[value='']").removeAttr("selected");
    $("#fecha_envio").val("");
    mapa.paquetesbyid=[];
    mapa.paquetes=[];
    $("#paquetes > ol.vertical").html("<div class=\"row-fluid\">\
                      <div >Ninguno</div>\
                  </div>");
    $("#id_documento option").each(function(){
      var documento=$(this);
      documento.removeAttr("disabled");
      documento.html(documento.html().replace(" añadido",""));
    });
  },
  iniciar:function(){  
    $("#miformulario").validator();
    $(".datepicker").datepicker();
    mapa.directionsService= new google.maps.DirectionsService();
    mapa.paquetesbyid[0]={};
    var myLatLng = {lat: 29.111286, lng: -110.981230};
    mapa.directionsDisplay = new google.maps.DirectionsRenderer();
    mapa.map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: myLatLng
        });
    mapa.directionsDisplay.setMap(mapa.map);
    mapa.directionsDisplay.setOptions( { suppressMarkers: true } );
    $("#id_documento").on("change",function(){
      var cliente=$("#id_documento option[value="+$(this).val()+"]").data("idcliente");
      mapa.obtenerdirecciones(cliente);
    });
    $('#miformulario').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
          general.alerta("Revise los campos",4);
        } else {
          mapa.guardar();
        }
        return false;
      });
    $("#guardar").on("click",function(){
      $("#miformulario").submit();
    });
    $("#cancelar").on("click",function(){
      mapa.limpiar();
    });
    $("#addpack").on("click",function(){
      $("#id_documento option[value='']").removeAttr("selected");
      $("#direccion option[value='']").removeAttr("selected");
      var vacio=mapa.paquetes.length>0;
      var direccion="";
      mapa.direcciones.forEach(function(value){
        if(value.id_direccion==$("#direccion").val())
          direccion=((value.direccion+" "+value.num_exterior+", "+value.colonia+", "+value.cp+", hermosillo, sonora "));
      });
      var bdireccion=false;
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': direccion }, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            mapa.apuntador=mapa.paquetes.length;
            mapa.paquetesbyid[$("#id_documento").val()]={};
            mapa.paquetes[mapa.apuntador]={};
            mapa.paquetes[mapa.apuntador]["id_documento"]=$("#id_documento").val();
            mapa.paquetesbyid[mapa.paquetes[mapa.apuntador]["id_documento"]]["lat"]=results[0].geometry.location.lat();
            mapa.paquetesbyid[mapa.paquetes[mapa.apuntador]["id_documento"]]["lng"]=results[0].geometry.location.lng();
            mapa.paquetesbyid[mapa.paquetes[mapa.apuntador]["id_documento"]]["marker"]=null;
            mapa.paquetes[mapa.apuntador]["lat"]=results[0].geometry.location.lat();
            mapa.paquetes[mapa.apuntador]["lng"]=results[0].geometry.location.lng();

            bdireccion=true;
          }
          else {
            general.alerta("No se encontro la dirección, intente con otra dirección",4);
            console.log("Geocoding failed: " + status);                           
          }
      }); 
      setTimeout(function(){
        if(bdireccion){
          if(!vacio)
            $("#paquetes > ol.vertical").html("");
          else
            $("ol.vertical").sortable("destroy");

          var documento=$("#id_documento option[value="+$("#id_documento").val()+"]").html();
          var direccion=$("#direccion option[value="+$("#direccion").val()+"]").html();
          
          var codhtml="  <li data-itemid='" +($("#id_documento").val())+"' class=\"row-ruta \"  ><div class=\"input-group\">\
                        <div >Documento : "+documento+"</div>\
                        <div >Dirección : "+direccion+"</div>\
                        <span class=\"input-group-btn \">\
                        <button type=\"button\" class=\"btn btn-secundary close\" style=\" position:left;\" >X</button>\
                        </span>\
                        </div></li>";
          $("#paquetes > ol.vertical").append(codhtml);
          $(".close").unbind();
          $(".close").on("click",function(){
            var li=$(this).parent().parent().parent().data("itemid");
            if(mapa.paquetes[mapa.paquetes.length-1]["marker"]!=null){
              mapa.paquetes[mapa.paquetes.length-1]["marker"].setMap(null);
              mapa.paquetes[mapa.paquetes.length-1]["marker"]=null;
            }
            mapa.paquetes.splice(mapa.paquetes.length-1,1);
            var nuevoarreglo=[];
            for(var i=0;i<mapa.paquetesbyid.length;i++){
              if(li!=i){
                nuevoarreglo[i]={};
                nuevoarreglo[i]=mapa.paquetesbyid[i];

              }
              else{
                if(mapa.paquetesbyid[i]["marker"]!=null){
                  mapa.paquetesbyid[i]["marker"].setMap(null);
                  mapa.paquetesbyid[i]["marker"]=null;
                }
              }
            }
            mapa.paquetesbyid=nuevoarreglo;
            var documento=$("#id_documento option[value="+li+"]");
            documento.removeAttr("disabled");
            documento.html(documento.html().replace(" añadido",""));
            $(this).parent().parent().parent().remove();
            if(!(mapa.paquetes.length>0)){
              $("#paquetes > ol.vertical").append("<div class=\"row-fluid\">\
                          <div >Ninguno</div>\
                      </div>");
            }
            mapa.ordenar();
            mapa.sortable();
          });
          setTimeout(function(){
              var documento=$("#id_documento option[value="+$("#id_documento").val()+"]");
              documento.html(documento.html()+" añadido");
              documento.attr("disabled","disabled");
              $("#id_documento option[value='']").attr("selected","selected");
              $("#direccion option[value='']").attr("selected","selected");
            },500);
          mapa.sortable();
        }
      },600);
    });
    $("#verruta").on("click",function(){
      if (mapa.directionsDisplay != null) {
        mapa.directionsDisplay.setMap(null);
        mapa.directionsDisplay = null;
      }
      mapa.directionsDisplay = new google.maps.DirectionsRenderer();
      mapa.directionsDisplay.setMap(mapa.map);
      mapa.directionsDisplay.setOptions( { suppressMarkers: true } );
      mapa.clearsmarkets();
      if(mapa.marker!=null){
        mapa.marker.setMap(null);
        mapa.marker=null;
      }
      if(mapa.paquetes.length>0){
        var position = {lat:29.111286 , lng: -110.981230};
        mapa.marker = new google.maps.Marker({
          position: position,
          map: mapa.map,
          icon: "/imagenes/inicio.png",
          title: 'Hello World!'
        });
        /*var feature = new ol.Feature({
          type: 'place',
          geometry: new ol.geom.Point(ol.proj.transform([-110.981230,29.111286], 'EPSG:4326',     
                'EPSG:3857'))
        });
        var style=new ol.style.Style({
          image: new ol.style.Icon({
            anchor: [0.5, 0.5],
            src: "/imagenes/inicio.png"
          })
        });
        feature.setStyle(style);
        mapa.vectorSource.addFeature(feature);*/
        var puntos=[];
        var firstpoint= {lat:29.111286 , lng: -110.981230};
        var endpoint=null;
        for(var x=0;x<mapa.paquetes.length;x++){
          position={lat:mapa.paquetes[(x)]["lat"] , lng: mapa.paquetes[(x)]["lng"]};
          mapa.paquetesbyid[mapa.paquetes[x]["id_documento"]]["marker"] = new google.maps.Marker({
            position: position,
            map: mapa.map,
            icon: "/imagenes/paquete.png",
            title: 'Hello World!'
          });
          if((x+1)==mapa.paquetes.length)
            endpoint=position;
          else
            puntos.push({
              location:position,
              stopover:true
              });
        }

        mapa.calcularruta(firstpoint,endpoint,puntos);
      }
      mapa.error=0;
    });
    $("#ver").on("click",function(){
      if (mapa.directionsDisplay != null) {
        mapa.directionsDisplay.setMap(null);
        mapa.directionsDisplay = null;
      }
      if(mapa.marker!=null){
        mapa.marker.setMap(null);
        mapa.marker=null;
      }
      mapa.clearsmarkets();
      if($("#direccion").val()!=""){
        var direccion="";
        mapa.direcciones.forEach(function(value){
          if(value.id_direccion==$("#direccion").val())
            direccion=((value.direccion+" "+value.num_exterior+", "+value.colonia+", "+value.cp+", hermosillo, sonora "));
        });
        var geocoder = new google.maps.Geocoder();
          geocoder.geocode({ 'address': direccion }, function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                var position = {lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng()};
                mapa.marker = new google.maps.Marker({
                  position: position,
                  map: mapa.map,
                  icon: "/imagenes/paquete.png",
                  title: 'Hello World!'
                });
                mapa.map.setCenter(position);
              }
              else {
                general.alerta("No se encontro la dirección, intente con otra dirección",4);
                console.log("Geocoding failed: " + status);                            
              }
          });
      }
    });
  },
  clearsmarkets:function(){
    mapa.paquetesbyid.forEach(function(valor, indice, array){
      if(indice!=0){
        if(mapa.paquetesbyid[indice]["marker"]!=null)
          mapa.paquetesbyid[indice]["marker"].setMap(null);
      }
    });
  },
  calcularruta:function(start,end,puntos) {
    console.log(start);
    console.log(end);
    console.log(puntos);
    var request = {
      origin: start,
      destination: end,
      waypoints: puntos,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING
    };
    mapa.directionsService.route(request, function (response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        mapa.directionsDisplay.setDirections(response);

      }
    });
  },   
  obtenerdirecciones:function(id_cliente){
    var funsuccess=function(data){
      if(data["tipo"]==1){
        var options="<option  value=''>Seleccione una direccion</option>";
        mapa.direcciones=data["datos"];
        data["datos"].forEach(function(value){
          options+="<option value='"+value.id_direccion+"'>"+(value.direccion+" "+value.num_exterior+", "+value.colonia)+"</option>";
        });
        $("#direccion").html(options);
        $("#direccion").removeAttr("disabled");
      }
      if(data["tipo"]==4){
        setTimeout(function(){ window.location.reload(true); },1000);
      }
    }
    var funcomplete=function(){

    }
    var funbefore=function(){

    }
    var funerror=function(){

    }
    general.cargarajax('/mapa/obtenerdirecciones',{id_cliente:id_cliente},funsuccess,funbefore,funcomplete,funerror,"json"); 
  },
  sortable:function(){
    $("ol.vertical").sortable({
      group: 'vertical',
      pullPlaceholder: false,
      // animation on drop
      onDrop: function  ($item, container, _super) {
        var $clonedItem = $('<li/>').css({height: 0});
        $item.before($clonedItem);
        $clonedItem.animate({'height': $item.height()});

        $item.animate($clonedItem.position(), function  () {
          $clonedItem.detach();
          _super($item, container);
        });
        mapa.ordenar();
      },
      onDragStart: function ($item, container, _super) {
        var offset = $item.offset(),
            pointer = container.rootGroup.pointer;

        mapa.adjustment = {
          left: pointer.left - offset.left,
          top: pointer.top - offset.top
        };

        _super($item, container);
      },
      onDrag: function ($item, position) {
        $item.css({
          left: position.left - mapa.adjustment.left,
          top: position.top - mapa.adjustment.top
        });
      }
    });
  },
  ordenar:function(){
    var cont=0;
    $("#paquetes > ol.vertical > li").each(function(){

      var id=$(this).data("itemid");
      if(typeof id != "undefined"){
        mapa.paquetes[cont]["id_documento"]=id;
        mapa.paquetes[cont]["lat"]=mapa.paquetesbyid[id]["lat"];
        mapa.paquetes[cont]["lng"]=mapa.paquetesbyid[id]["lng"];
        cont++;
      }
    });
  }  
};
      
      
