var dashboard={
	docxrep:[],
	gastosxveh:[],
	gastosxemp:[],
	iniciar:function(){
		var alabels=[];
		var adata=[];
		dashboard.docxrep.forEach(function(value){
			alabels.push(value.nombre);
			adata.push(value.num_docs);
		});
		dashboard.cargarchart("reps_docs",alabels,adata,"# documentos completados");
		alabels=[];
		adata=[];
		dashboard.gastosxemp.forEach(function(value){
			alabels.push(value.nombre);
			adata.push(value.gasto);
		});
		dashboard.cargarchart("gastos_emp",alabels,adata,"gastos");
		alabels=[];
		adata=[];
		dashboard.gastosxveh.forEach(function(value){
			alabels.push("# "+value.num_vehiculo);
			adata.push(value.gasto);
		});
		dashboard.cargarchart("gastos_veh",alabels,adata,"gastos");
	},
	cargarchart:function(id_canva,alabels,adata,title){
		var ctx = document.getElementById(id_canva).getContext('2d');
		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: alabels,
		        datasets: [{
		            label: title,
		            data: adata,
		            backgroundColor: [
		                'rgba(255, 99, 132, 0.2)',
		                'rgba(54, 162, 235, 0.2)',
		                'rgba(255, 206, 86, 0.2)',
		                'rgba(75, 192, 192, 0.2)',
		                'rgba(153, 102, 255, 0.2)',
		                'rgba(255, 159, 64, 0.2)'
		            ],
		            borderColor: [
		                'rgba(255,99,132,1)',
		                'rgba(54, 162, 235, 1)',
		                'rgba(255, 206, 86, 1)',
		                'rgba(75, 192, 192, 1)',
		                'rgba(153, 102, 255, 1)',
		                'rgba(255, 159, 64, 1)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		});
	}
}