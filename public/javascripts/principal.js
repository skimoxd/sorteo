var principal={
	elegidos:0,
	repeticiones:0,
	cantidad:0,
	iniciar:function(){
		$('.carousel').carousel({
			interval:2000
		})
		principal.elegidos=0;
		$(".oculto").hide();
        $("#BtnPart").click(function(){
            if($(".oculto").is(":visible"))
            	$(".oculto").hide();
            else
            	$(".oculto").show();
            return false;
        });
        $(".bltpdr").click(function(){
        	//ids=$(this).data('ids');
        	//val=$(this).data('val');
        	//principal.obtenerFormulario(ids,val);
        	
        	if($(this).hasClass( "badge-chosen" )){
        		principal.elegidos--;
        		$(this).removeClass('badge-chosen');
        	}
        	else{
        		if(principal.elegidos>=10){
	        		general.alerta("No se pueden elegir mas de 10 boletos",3);
	        	}
	        	else{
        		principal.elegidos++;
        		$(this).addClass('badge-chosen');
        		}
        	}
        });
        $("#BtnPedir").click(function(){
        	principal.cantidad=$(this).data("cantidad");
        	if(principal.elegidos<=0)
        		general.alerta("Seleccione al menos un boleto",3);
        	else
        		principal.obtenerFormulario();
        	return false;
        });
	},
	obtenerFormulario:function(ids,val){
		var sorteo=0;
		var obtenerF=1;
		var boletos="";
		var oportunidades="";
		principal.repeticiones=(10000/principal.cantidad);
		var controller=$("#new").data('controller');
		$( ".badge-chosen" ).each(function() {
			if(sorteo!=0){
				if(sorteo!=$( this ).data( "ids" ))
				{
					general.alerta("No se pueden tener boletos seleccionados de rifas diferentes",3);
					obtenerF=0;
				}
			}
			sorteo=$( this ).data( "ids" );
		});
		if(obtenerF){
			var funsuccess=function(data){
				$('#Formulario #contenido').html(data);
				$('#Formulario').modal();
			}
			var funcomplete=function(){
				$("#titulo").html("Formulario de información");
				setTimeout(function(){
					$( ".badge-chosen" ).each(function() {
						$("#miformulario").append("<input name=\"num_b\" type=\"hidden\" value=\""+$( this ).data( "val" )+"\">");
						if(boletos!="")
							boletos+=','+ ('0000'+$( this ).data( "val" )).slice(-4);
						else
							boletos= ('0000'+$( this ).data( "val" )).slice(-4);
						for(var x=0;x<principal.repeticiones;x++){
							if(oportunidades!="")
								oportunidades+=','+ ('0000'+($( this ).data( "val" )+(principal.cantidad*x))).slice(-4);
							else
								oportunidades= ('0000'+($( this ).data( "val" )+(principal.cantidad*x))).slice(-4);
						}
					});
					$("#miformulario").append("<input name=\"id_s\" type=\"hidden\" value=\""+sorteo+"\">");
					$("#num_boletos").val(boletos);
					$("#num_oportunidades").val(oportunidades);
					principal.cargarvalidacion();
			        $('.chosen-select').chosen();
					$('.chosen-select-deselect').chosen();

				},500);
			}
			var funbefore=function(){

			}
			var funerror=function(){

			}
			general.cargarajax('/boleto/pedirboleto',{},funsuccess,funbefore,funcomplete,funerror,"html");
		}
	},
	guardarboleto:function(datos){
		if($("#guardar").attr('disabled')!="disabled"){
			$("#guardar").attr('disabled','disabled');
			var funsuccess=function(data){
				if(data["tipo"]==1){
					general.alerta("Su boleto ha sido apartado exitosamente, se comunicaran con usted en los proximos 30 minutos",1);
					setTimeout(function(){ window.location.reload(true); },2000);	
				}
				if(data["tipo"]==3){
					general.alerta("Su boleto ha sido apartado exitosamente<br>"+data["mensaje"],1);
					setTimeout(function(){ window.location.reload(true); },10000);	
				}
				if(data["tipo"]==2){
					mensaje="No se pudo apartar el boleto <br>Detalles: "+data["mensaje"];
					general.alerta(mensaje,4);
					$("#guardar").removeAttr('disabled');
				}
				if(data["tipo"]==4){
					setTimeout(function(){ window.location.reload(true); },1000);
				}
			}
			var funcomplete=function(){
				
			}
			var funbefore=function(){

			}
			var funerror=function(){

			}
			general.cargarajax('/boleto/guardarboleto',{'datos':datos},funsuccess,funbefore,funcomplete,funerror,"json");
	}
	},
	cargarvalidacion:function(){
		$("#guardar").on('click',function(){
			$('#miformulario').validator().on('submit', function (e) {
			  	if (e.isDefaultPrevented()) {
			    	
			  	} else {
					var form=$("#miformulario").serializeObject();
					principal.guardarboleto(form);

			  	}
			  	return false;
			})
			$("#miformulario").submit();
		});
		$("#miformulario").validator();
	}
}