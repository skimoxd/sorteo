var chequeo={
	producto:"",
	agregados:{},
	insertarhtml:true,
	iniciar:function(){
		$("#miformulario").validator();
		$('#miformulario').validator().on('submit', function (e) {
		  	if (e.isDefaultPrevented()) {
		  		console.log(e);
		    	general.alerta("Checa los campos",4);
		  	} else {
		  		if(Object.keys(chequeo.agregados)==0)
		  			general.alerta("Agregue al menos un articulo",4);
		  		else{
					var form=$("#miformulario").serializeObject();
					chequeo.checar();
				}
					
		  	}
		  	return false;
		});
		$("#guardar").on('click',function(){
			$("#miformulario").submit();
		});
		$( "#nomenclatura" ).keypress(function(e) {
			if(e.which == 13) {
		        chequeo.producto=$(this).val();
				chequeo.buscarproducto(chequeo.producto);
				$(this).val("");
				chequeo.producto="";
		    }
		});
	},
	checar:function(){
		var form=$("#miformulario").serializeObject();
		var funsuccess=function(data){
			if(data=="reinicio")
				setTimeout(function(){ window.location.reload(true); },1000);
			else{
				if(data=="Chequeo completo"){
					general.alerta(data,1);
					setTimeout(function(){ window.location.reload(true); },1500);
				}
				else{
					general.alerta(data,4);
				}
			}
		}
		var funcomplete=function(){
			
		}
		var funbefore=function(){
		}
		var funerror=function(){

		}
		general.cargarajax('/chequeo/checar',{datos:form},funsuccess,funbefore,funcomplete,funerror,"html");
	},
	buscarproducto:function(producto){
		if(producto.trim().length>0){
			var funsuccess=function(data){
				if(data["tipo"]==1){
			        chequeo.agregarproducto(data["mensaje"]);
			    }
			    if(data["tipo"]==2){
			        general.alerta("No se encontro productos con el codigo "+producto,4);
			    }
			    if(data["tipo"]==4){
			    	setTimeout(function(){ window.location.reload(true); },1000);
			    }
			}
			var funcomplete=function(){
				
			}
			var funbefore=function(){
			}
			var funerror=function(){

			}
			general.cargarajax('/chequeo/obtenerproducto',{clave:producto},funsuccess,funbefore,funcomplete,funerror,"json");
		}
		else
			general.alerta("No se encontro productos con el codigo "+producto,4);
		
	},
	agregarproducto:function(producto){
		if(typeof chequeo.agregados[producto["id_producto"]]=="undefined")
			chequeo.agregados[producto["id_producto"]]=1;
		else
			chequeo.agregados[producto["id_producto"]]=chequeo.agregados[producto["id_producto"]]+1;

		$("#div_producto .row").each(function(){
			var id=$(this).find("input:eq(0)"),elemento="";
			if(typeof id.val() !="undefined"){
				if(parseInt(id.val())==producto["id_producto"]){
					elemento=$(this).find("input:eq(2)");
					elemento.val(chequeo.agregados[producto["id_producto"]]);
					elemento=$(this).find("input:eq(3)");
					elemento.val(chequeo.agregados[producto["id_producto"]]);
					chequeo.insertarhtml=false;
				}
			}
		});
		if(chequeo.insertarhtml){
			$("#div_producto").append("\
				<div class=\"row\">\
					<div class=\"form-group  col-md-5 col-md-offset-2\">\
						<input type=\"hidden\" name=\"id_producto\" value=\""+producto["id_producto"]+"\" >\
						<input type=\"text\" class=\"form-control\" id=\"nomenclatura\" value=\""+producto["codigo1"]+" "+producto["descripcion"]+"\" name=\"nomenclatura\" placeholder=\"Nomenclatura\" disabled>\
					</div>\
					<div class=\"form-group  col-md-3\">\
						<input type=\"hidden\" class=\"form-control calculo\" id=\"cantidad\" name=\"cantidad\" value=\""+chequeo.agregados[producto["id_producto"]]+"\"  placeholder=\"cantidad\">\
						<input type=\"text\" class=\"form-control calculo\" id=\"cantidad\"  value=\""+chequeo.agregados[producto["id_producto"]]+"\" disabled placeholder=\"cantidad\">\
					</div>\
					<div class=\"form-group col-md-2\">\
						<div class=\"input-group\">\
							<span class=\"input-group-btn closeprod\">\
								<button type=\"button\" class=\"btn btn-secundary \" id=\"close closeprod \">X</button>\
							</span>\
						</div>\
					</div>\
				</div>");
		}
		setTimeout(function(){
			$(".closeprod").on("click",function(){
				$(this).parent().parent().parent().remove();
				id=$(this).parent().parent().parent().find("input:eq(0)").val();
				delete chequeo.agregados[id]; 
			}); 
		},500);
		chequeo.insertarhtml=true;
	}
}