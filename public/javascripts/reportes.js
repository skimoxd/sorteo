var reportes={
	iniciar:function(){
        $('.input-daterange').datepicker({
        	language:"es",
        	keyboardNavigation: false,
        	forceParse: false
        });
		setTimeout(function(){
			$('.chosen-select').chosen({ width: '100%' });
		    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
		    $("#consulta").on("click",function(){
		    	reportes.consulta();
		    	return false;
		    });
		    $("#consulta2").on("click",function(){
		    	reportes.consulta2();
		    	return false;
		    });
		    
		},500);
	},
	consulta:function(){
		var datos=[];
		var path="/reporte/obtenerreporte";
		var rifa=$("#rifa").val().join(",");
		datos["id_sorteo"]=rifa;
		general.post(path,datos);
	},
	consulta2:function(){
		var datos=[];
		var path="/reporte/obtenerreporte2";
		datos["fecha1"]=$("#fecha1").val();
		datos["fecha2"]=$("#fecha2").val();
		general.post(path,datos);
	}
}