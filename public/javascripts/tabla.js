var tabla={
	generar:function(){
		var table="";
		if(typeof cliente=="object" || typeof transportista =="object"){
			eval("table=$('.dTable').DataTable({\
				"+((typeof nomina=="object")?"lengthChange: false,\
        		buttons: [  'excel', 'pdf' ],":"")+"\
				\"language\":{\
				    \"sProcessing\":     \"Procesando...\",\
				    \"sLengthMenu\":     \"Mostrar _MENU_ registros\",\
				    \"sZeroRecords\":    \"No se encontraron resultados\",\
				    \"sEmptyTable\":     \"Ningún dato disponible en esta tabla\",\
				    \"sInfo\":           \"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros\",\
				    \"sInfoEmpty\":      \"Mostrando registros del 0 al 0 de un total de 0 registros\",\
				    \"sInfoFiltered\":   \"(filtrado de un total de _MAX_ registros)\",\
				    \"sInfoPostFix\":    \"\",\
				    \"sSearch\":         \"Buscar:\",\
				    \"sUrl\":            \"\",\
				    \"sInfoThousands\":  \",\",\
				    \"sLoadingRecords\": \"Cargando...\",\
				    \"oPaginate\": {\
				        \"sFirst\":    \"Primero\",\
				        \"sLast\":     \"Último\",\
				        \"sNext\":     \"Siguiente\",\
				        \"sPrevious\": \"Anterior\"\
				    },\
				    \"oAria\": {\
				        \"sSortAscending\":  \": Activar para ordenar la columna de manera ascendente\",\
				        \"sSortDescending\": \": Activar para ordenar la columna de manera descendente\"\
				    }\
				},\
				\"columnDefs\": [\
		            {\
		            	 targets: [ 5 ],\
		                \"orderable\":      false\
		            }\
		            \
		        ],\
				responsive: true\
			})");
			
			$('.dTable tbody').on('click', 'div.details-control', function () {
		        var tr = $(this).closest('tr');
		        var row = $(".dTable").DataTable().row( tr );
		 		
		        if ( row.child.isShown() ) {
		            // This row is already open - close it
		            row.child.hide();
		            tr.removeClass('shown');
		        }
		        else {
		            // Open this row
		            if(typeof cliente=="object" )
		           		row.child( cliente.cargarExtra($(this).data("id")) ).show();
		           	if(typeof transportista =="object")
		           		row.child( transportista.cargarExtra($(this).data("id")) ).show();
		            tr.addClass('shown');
		        }
		    } );
		}
		else{
			eval("table=$('.dTable').DataTable({\
				"+((typeof nomina=="object" || typeof empleado=="object")? ((typeof nomina=="object")?"lengthChange: false,":"")+"\
        		buttons: [  'excel', 'pdf' ],":"")+"\
				\"language\":{\
				    \"sProcessing\":     \"Procesando...\",\
				    \"sLengthMenu\":     \"Mostrar _MENU_ registros\",\
				    \"sZeroRecords\":    \"No se encontraron resultados\",\
				    \"sEmptyTable\":     \"Ningún dato disponible en esta tabla\",\
				    \"sInfo\":           \"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros\",\
				    \"sInfoEmpty\":      \"Mostrando registros del 0 al 0 de un total de 0 registros\",\
				    \"sInfoFiltered\":   \"(filtrado de un total de _MAX_ registros)\",\
				    \"sInfoPostFix\":    \"\",\
				    \"sSearch\":         \"Buscar:\",\
				    \"sUrl\":            \"\",\
				    \"sInfoThousands\":  \",\",\
				    \"sLoadingRecords\": \"Cargando...\",\
				    \"oPaginate\": {\
				        \"sFirst\":    \"Primero\",\
				        \"sLast\":     \"Último\",\
				        \"sNext\":     \"Siguiente\",\
				        \"sPrevious\": \"Anterior\"\
				    },\
				    \"oAria\": {\
				        \"sSortAscending\":  \": Activar para ordenar la columna de manera ascendente\",\
				        \"sSortDescending\": \": Activar para ordenar la columna de manera descendente\"\
				    }\
				},\
				"+((typeof venta =="object")?"":
				"responsive: true")
			+"})");
			
		}
		if((typeof nomina=="object" || typeof empleado=="object")){
			setTimeout(function(){table.buttons().container().appendTo( '.dataTables_wrapper .col-sm-6:eq(0)' )},500);
		}
		setTimeout(function(){
			general.loading();
		},1000);
	},
	refresh:function(){
		
	}
}