<div class="nav-side-menu">
<nav class="navbar navbar-default" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only"><?=$this->setcode("Navegador");?></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#" ><?=$this->setcode("Menu");?></a>
  </div>
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li <?=( ($_SERVER['REQUEST_URI']=='/index'))?'class="active"':'';?> ><a href="/index"><?=$this->setcode("Dasboard");?></a></li>
      <hr>
      <li <?=( ($_SERVER['REQUEST_URI']=='/boleto'))?'class="active"':'';?> ><a href="/boleto"><?=$this->setcode("Ingresar Boleto");?></a></li>
      <li <?=(strpos($_SERVER['REQUEST_URI'],'/usuario')!==false)?'class="active"':'';?>><a href="/usuario"><?=$this->setcode("Usuarios");?></a></li>
      <li <?=(strpos($_SERVER['REQUEST_URI'],'/cuenta')!==false)?'class="active"':'';?>><a href="/cuenta"><?=$this->setcode("Cuenta");?></a></li>
      <li <?=(strpos($_SERVER['REQUEST_URI'],'/rifa')!==false)?'class="active"':'';?>><a href="/rifa"><?=$this->setcode("Rifas");?></a></li>
      <li <?=(($_SERVER['REQUEST_URI']=='/abono'))?'class="active"':'';?>><a href="/abono"><?=$this->setcode("Registrar Abonos");?></a></li>
      <li <?=(strpos($_SERVER['REQUEST_URI'],'/reporte/boletos')!==false)?'class="active"':'';?>><a href="/reporte/boletos"><?=$this->setcode("Reporte Boletos");?></a></li>
      <li <?=(strpos($_SERVER['REQUEST_URI'],'/reporte/abonos')!==false)?'class="active"':'';?>><a href="/reporte/abonos"><?=$this->setcode("Reporte Abonos");?></a></li>
      <hr>
      <li class="logout"><a href="/login/logout"><span class="glyphicon glyphicon-off"></span> <?=$this->setcode("Cerrar Sesion");?></a></li>
    </ul>
  </div>
</nav>